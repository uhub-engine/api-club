'use strict';

const _frb = require('../../server/services/firebase');
const pagarme = require('../../server/services/pagarme');
const _utl = require('../../server/helpers/utils');
const multiparty = require('multiparty');
const _aws = require('../../server/services/aws.js');

module.exports = function(Admin) {

/**
 * Authenticate user to access data on admin application
 * @param {object} data The data from user request
 * @param {Function(Error, object)} callback
 */
Admin.authenticate = async function(data, callback) {
    const oResponse = await _frb.verifyIdToken(data.accessToken);
    if(oResponse){

        let oUser = await Admin.app.models.Customer.findOne({where:{uid:data.uid}});
        if(!oUser){
            var err = new Error("Missing required level");
            err.name = "We not find your user on our application, please register first";
            err.statusCode = 401;
            return callback(err)
        }
        
        let filter = {
            where:{
                and:[
                    {
                        team:{
                             inq: [oUser.uid]
                        }
                    },
                    {host: data.host}
                ]
            }
            
        };

        // Check user its not admin, verify if he had access to this area by participating from team by host request.
        if(oUser.isAdmin){
            filter = {where:{host:data.host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);

        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path, we are registering your request data to investigate";
            err.statusCode = 401;
            return callback(err)
        }

        let oAdminUser = await Admin.app.models.CustomerAccessToken.findOne({where:{id:data.accessToken}});

        if(!oAdminUser){
            oAdminUser = await Admin.app.models.CustomerAccessToken.create({
                id: data.accessToken,
                ttl: 1209600,
                created: new Date(),
                userId: oUser.id
            });
        }

        return {
            id: oAdminUser.id,
            clientId: oClient.id
        };
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path, we are registering your request data to investigate";
    err.statusCode = 401;
    return callback(err)
};

/**
 * Create new institution by admins
 * @param {object} data The data to create new institution
 * @param {Function(Error, object)} callback
 */
Admin.createInstitution = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient();
    
    if(oCustomer.isAdmin){
        const oInstitution = await Admin.app.models.Client.create(data);

        let oActionTemplate = {
            date: new Date(),
            type: 'success',
            data: {
                message: 'Create Institution',
                data: oInstitution
            },
            customerId: oCustomer.id,
            clientId: oClient.id
        }
    
        if(!oInstitution){
            oActionTemplate.type = 'danger';
        }
        
        // Admin.app.models.ActionHistory.create(oActionTemplate);

        return oInstitution;
    }
    
    var err = new Error("Missing required level");
    err.name = "You cannot request that path, we are registering your request data to investigate";
    err.statusCode = 401;
    return callback(err)
};

/**
 * Detail institution requesting by host
 * @param {string} host The host from institution to verify wich instition domain request
 * @param {Function(Error, object)} callback
 */
Admin.detailInstitution = async function(options, host, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    
    let filter = {
        where:{
            and:[
                {
                    team:{
                         inq: [oCustomer.uid]
                    }
                },
                {host: host}
            ]
        }
    }

    if(oCustomer.isAdmin){
        filter = {where:{host}};
    }

    const oClient = await Admin.app.models.Client.findOne(filter);

    if(!oClient){
        var err = new Error("Missing required level");
        err.name = "You cannot request that path, we are registering your request data to investigate";
        err.statusCode = 401;
        return callback(err)
    }

    let oActionTemplate = {
        date: new Date(),
        type: 'success',
        data: {
            message: 'Detail Institution',
            data: oClient
        },
        customerId: oCustomer.id,
        clientId: oClient.id
    }

    if(!oClient){
        oActionTemplate.type = 'danger';
    }
    
    // Admin.app.models.ActionHistory.create(oActionTemplate);

    return oClient;
};

/**
 * This method its simple way to update the institution details
 * @param {object} data The data with the entire institution detail
 * @param {Function(Error, object)} callback
 */
Admin.updateInstitutionConfiguration = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;

    if(oCustomer && host){
        
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);

        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        var name = typeof(data.name) == 'string' && data.name.length > 0 ? data.name : false;
        if(name) oClient.name = name; 
        var logo = typeof(data.logo) == 'string' && data.logo.length > 0 ? data.logo : false;
        if(logo) oClient.logo = logo;
        var logomark = typeof(data.logomark) == 'string' && data.logomark.length > 0 ? data.logomark : false;
        if(logomark) oClient.logomark = logomark;
        var contact_email = typeof(data.contact_email) == 'string' && data.contact_email.length > 0 ? data.contact_email : false;
        if(contact_email) oClient.contact_email = contact_email; 
        var contact_phone = typeof(data.contact_phone) == 'string' && data.contact_phone.length > 0 ? data.contact_phone : false;
        if(contact_phone) oClient.contact_phone = contact_phone; 

        return await oClient.save();
    }


    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
};

/**
 * The method create single user 
 * @param {object} data The object must contain all the required data to create user
 * @param {Function(Error, object)} callback
 */
Admin.createUser = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const name = typeof(data.name) == 'string' && data.name.length > 0 ? data.name : false;
    const code = typeof(data.code) == 'string' && data.code.length > 0 ? data.code : false;
    
    if(host && name && code){

        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);

        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }
 
        let oUser = {
            name,
            code,
            email: `${new Date().getTime()}@latinoeuropeu.com`
        }
                
        // Verify if user exists to append oClient.id or create new user and append passport
        let oCreatedUser = await Admin.app.models.Customer.findOne({where:{code}});
        if(!oCreatedUser){
            oUser.password = new Date().toTimeString();
            oUser.passport = [oClient.id];
            oCreatedUser = await Admin.app.models.Customer.create(oUser);
        }
        
        if(typeof(oCreatedUser.passport) == 'object' && oCreatedUser.passport instanceof Array){
            var clientId = oClient.id.toString();
            var hasOnPassport = await oCreatedUser.passport.filter(oClientId => oClientId.toString() == clientId);
            if(hasOnPassport.length < 1){
                oCreatedUser.passport.push(oClient.id);
                await oCreatedUser.save();
            }
        } else {
            oCreatedUser.passport = [oClient.id];
            await oCreatedUser.save();
        }

        return oUser; 
    }


    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
};

/**
 * This method list all users from some host
 * @param {string} host The host from the requester to process as admin
 * @param {Function(Error, array)} callback
 */
Admin.listUsers = async function(options, host, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;

    if(oCustomer && host){
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
       
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const aCustomer = await Admin.app.models.Customer.find({
            where: {
                passport: {
                    inq: [oClient.id]
                }
            },
            include: [
                "transactions"
            ]
        }); 

        let oActionTemplate = {
            date: new Date(),
            type: 'success',
            data: {
                message: 'Listing Users',
                data: aCustomer
            },
            customerId: oCustomer.id,
            clientId: oClient.id
        }

        if(!aCustomer){
            oActionTemplate.type = 'danger';
        }
        
        // Admin.app.models.ActionHistory.create(oActionTemplate);

        return aCustomer;
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err);
};

/**
 * This method is to create new product on institution
 * @param {object} data Data to create product
 * @param {Function(Error, object)} callback
 */
Admin.createProduct = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const name = typeof(data.name) == 'string' && data.name.length > 0 ? data.name : false; 
    const description = typeof(data.description) == 'string' && data.description.length > 0 ? data.description : false; 
    const price = typeof(data.price) == 'string' && data.price > 0 ? (+data.price * 100) : false; 
    const date = typeof(data.date) == 'string' && data.date.length > 0 ? new Date(data.date) : false;
    const dateend  = typeof(data.dateend) == 'string' && data.dateend.length > 0 ? new Date(data.dateend) : false;
    const duration = typeof(data.duration) == 'string' && data.duration > 0 ? data.duration : 1;
    const checkDefault = typeof(data.default) == 'boolean' ? data.default : false;
    const checkAmnesty = typeof(data.amnesty) == 'boolean' ? data.amnesty : false;
    const sProductId = typeof(data.productId) == 'string' ? data.productId : false;

    if(oCustomer && host && name && price && date && dateend){

        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);

        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const oProductTemplate = {
            name,
            description,
            price,
            date,
            dateend,
            duration,
            default: checkDefault,
            amnesty: checkAmnesty,
            clientId: oClient.id
        }

        const hasDefaultProductAlready = await Admin.app.models.Product.find({where:{and:[{clientId: oClient.id},{default:true}]}});
        if(hasDefaultProductAlready && checkDefault){
            hasDefaultProductAlready.map(oProduct => {
                oProduct.default = false;
                oProduct.save();
            })
        }

        let oProduct; 

        if(sProductId){
            oProduct = await Admin.app.models.Product.findOne({where:{id:sProductId}})
            oProduct['name'] = name;
            oProduct['description'] = description;
            oProduct['price'] = price;
            oProduct['date'] = date;
            oProduct['dateend'] = dateend;
            oProduct['duration'] = duration;
            oProduct['default'] = checkDefault;
            oProduct['amnesty'] = checkAmnesty;
            await oProduct.save()
        } else {
            oProduct = await Admin.app.models.Product.create(oProductTemplate); 
        }

        
        // let oActionTemplate = {
        //     date: new Date(),
        //     type: 'success',
        //     data: {
        //         message: 'Creating Product',
        //         data: oProduct
        //     },
        //     customerId: oCustomer.id,
        //     clientId: oClient.id
        // }

        // if(!oProduct){
        //     oActionTemplate.type = 'danger';
        // }
        
        // Admin.app.models.ActionHistory.create(oActionTemplate);
        
        return oProduct; 
    }


    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
};

/**
 * This method lists all the products into institution
 * @param {string} host The host from application
 * @param {Function(Error, array)} callback
 */
Admin.listProducts = async function(options, host, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;

    let oActionTemplate = {};

    if(oCustomer && host){
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
       
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const aProduct = await Admin.app.models.Product.find({where:{clientId:oClient.id}}); 

        if(!aProduct){
            oActionTemplate = {
                date: new Date(),
                type: `danger`,
                data: {
                    message: 'Listing Products',
                    data: aProduct
                },
                customerId: oCustomer.id,
                clientId: oClient.id
            }
        }
        
        oActionTemplate = {
            date: new Date(),
            type: `success`,
            data: {
                message: 'Listing Products',
                data: aProduct
            },
            customerId: oCustomer.id,
            clientId: oClient.id
        }
        
        // Admin.app.models.ActionHistory.create(oActionTemplate);


        return aProduct;
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err);
};

/**
 * This method handle the post of multiple user from upload file into front admin application
 * @param {object} data The entire request must contains the host from application also the users list data
 * @param {Function(Error, object)} callback
 */
Admin.uploadUsersList = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const aListUsers = typeof(data.bulk) == 'string' && data.bulk.length > 9 ? JSON.parse(data.bulk) : false;


    if( oCustomer && aListUsers && host ){

        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);

        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        for (let index = 0; index < aListUsers.length; index++) {
            const oUser = aListUsers[index];
            
            const email = `${_utl.generateRandomFloatInRange(100000,999999999999)}@gmail.com`;
            const code = typeof(oUser.code) == 'string' && oUser.code.length > 0 ? oUser.code : false;

            let oActionTemplate = {
                date: new Date(),
                type: `danger`,
                data: {
                    message: `Não recebeu o usuário com e-mail válido`,
                    data: oUser
                },
                customerId: oCustomer.id,
                clientId: oClient.id
            }
            
            if(code){
                
                // Verify if user exists to append oClient.id or create new user and append passport
                let oCreatedUser = await Admin.app.models.Customer.findOne({where:{code}});
                if(!oCreatedUser){
                    oUser.password = new Date().toTimeString();
                    oUser.passport = [oClient.id];
                    oCreatedUser = await Admin.app.models.Customer.create(oUser);
                    oActionTemplate.type = 'success';
                    oActionTemplate.data =  oCreatedUser;
                }
                
                if(typeof(oCreatedUser.passport) == 'object' && oCreatedUser.passport instanceof Array){
                    var clientId = oClient.id.toString();
                    var hasOnPassport = await oCreatedUser.passport.filter(oClientId => oClientId.toString() == clientId);
                    if(hasOnPassport.length < 1){
                      oCreatedUser.passport.push(oClient.id);
                      await oCreatedUser.save();
                    }
                  } else {
                    oCreatedUser.passport = [oClient.id];
                    await oCreatedUser.save();
                  }

            }


            // Admin.app.models.ActionHistory.create(oActionTemplate);
            
        }
        
        return {
            message : `Processamento do upload de usuários foi iniciado com sucesso, o processo pode durar até ${((aListUsers.length * 2) / 60)} minutos.`
        }; 
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
};

/**
 * List all transactions from institution
 * @param {string} host The institution host
 * @param {Function(Error, object)} callback
 */
Admin.listAllTransactions = async function(options, host, callback) {
    // console.log('adicionando console apenas para salvar e subir')
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;

    if(oCustomer && host){
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
       
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const aTransaction = await Admin.app.models.Transaction.find({where:{clientId: oClient.id},include:[{"customer":["transactions"]}]});
        return aTransaction;
    }
    

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err);
    
};

/**
 * Process the payments with status waiting_payment to verify if something new has happen
 * @param {string} host The host from application
 * @param {Function(Error, object)} callback
 */
Admin.processPayments = async function(options, host, callback) {
    
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;
    
    if(oCustomer && host){
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }
        
        const oClient = await Admin.app.models.Client.findOne(filter);
        
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }
        
        const aTransaction = await Admin.app.models.Transaction.find({where:{and:[{clientId: oClient.id},{processing: 'waiting'}]}, limit: 250, order: 'date DESC'});
        for (let index = 0; index < aTransaction.length; index++) {
            const oTransaction = aTransaction[index];
            console.log(oTransaction.type)
            if(typeof(oTransaction.tx) == 'object' && oTransaction.tx.id){
                const oCapturedTransaction = await new pagarme(Admin.app.dataSources.pagarmeDs).getx(oTransaction.tx.id);
                if(oTransaction.status != oCapturedTransaction.status){

                    const oTransactionTemplate = {
                        amount: oCapturedTransaction.paid_amount,
                        status: oCapturedTransaction.status,
                        date: oCapturedTransaction.date_updated,
                        type: oCapturedTransaction.payment_method,
                        clientId: oTransaction.clientId,
                        customerId: oTransaction.customerId,
                        ref: oTransaction.ref,
                        link: oTransaction.ref,                    
                        tx: oCapturedTransaction
                    };
                    
                    const oTx = await Admin.app.models.Transaction.create(oTransactionTemplate);

                    if(typeof(oTransaction.inscriptions) && oTransaction.inscriptions){
                        const oInscription = await oTransaction.inscriptions.get(); 

                        if(oInscription){
                            oInscription.transactionId = oTx.id;
                            await oInscription.save();
                        }
                    }

                    oTransaction.processing = 'done';
                    let resp = await oTransaction.save();
                    console.log(resp.id.toString(), 'done')
                } else {
                    oTransaction.processing = 'noc';
                    let resp = await oTransaction.save();
                    console.log(resp.id.toString(), 'noc')
                }
            } else {
                
                let oActionTemplate = {
                    date: new Date(),
                    type: 'warning',
                    data: {
                        message: 'Processing Transaction',
                        data: {
                            oTransaction
                        }
                    },
                    customerId: oCustomer.id,
                    clientId: oClient.id
                }
                
                await Admin.app.models.ActionHistory.create(oActionTemplate);
                
            }
            
        }
        return {
            message: `${aTransaction.length} transações processadas.`
        };
    }
    

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err);
    
};

/**
 * This method we use to create new congress into institution environment
 * @param {object} data The data contain all the information to create new congress
 * @param {Function(Error, object)} callback
 */
Admin.createCongress = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const name = typeof(data.name) == 'string' && data.name.length > 0 ? data.name : false;
    const type = typeof(data.type) == 'string' && data.type.length > 0 ? data.type : false;
    const date = typeof(data.date) == 'string' && data.date.length > 0 ? new Date(data.date) : false;
    const dateEnd = typeof(data.dateEnd) == 'string' && data.dateEnd.length > 0 ? new Date(data.dateEnd) : false;
    const url = typeof(data.url) == 'string' && data.url.length > 0 ? data.url : false; 

    const cutNote = typeof(data.cutNote) == 'string' && data.cutNote.length > 0 ? +data.cutNote : typeof(data.cutNote) == 'number' ? data.cutNote : 0; 
    const worksByCateg = typeof(data.worksByCateg) == 'string' && data.worksByCateg.length > 0 ? +data.worksByCateg : typeof(data.worksByCateg) == 'number' ? data.worksByCateg : 0;
    
    const sCongressId = typeof(data.congressId) == 'string' ? data.congressId : false;

    // Verification to data will be added on edit
    const payment = {
        boleto: typeof(data.boleto) == 'boolean' ? data.boleto : false, 
        credit_card: typeof(data.credit_card) == 'boolean' ? data.credit_card : false,
        pix: typeof(data.pix) == 'boolean' ? data.pix : false
    };
    const partnerFree = typeof(data["partner-free"]) == 'boolean' ? data["partner-free"] : false; 
    
    let aPriceCateg = _utl.processAllCheckBoxesByKey(data, 'price_');
    const resumeCongress = typeof(data.resumeCongress) == 'string' && data.resumeCongress.length > 0 ? data.resumeCongress : false;
    const topImageUrl = typeof(data.topImageUrl) == 'string' && data.topImageUrl.length > 0 ? data.topImageUrl : false;
    const status = typeof(data.status) == 'string' && data.status.length > 0 ? data.status : 'waiting';

    // Advanced
    const subdivision = typeof(data.subdivision) == 'boolean' ? data.subdivision : false;
    const subdivs = subdivision ? _utl.inputCheckboxToArray(data, 'sbdiv') : [];
    const categories = _utl.inputCheckboxToArray(data, 'categ');
    const iesList =  _utl.processAllCheckBoxesByKey(data, 'listies_');


    // Certification
    const certImageUrl = typeof(data.certImageUrl) == 'string' && data.certImageUrl.length > 0 ? data.certImageUrl : false;
    const congressmanCert = typeof(data.congressmanCert) == 'string' && data.congressmanCert.length > 0 ? data.congressmanCert : false;
    const avaliatorCert = typeof(data.avaliatorCert) == 'string' && data.avaliatorCert.length > 0 ? data.avaliatorCert : false;
    const coordinatorCert = typeof(data.coordinatorCert) == 'string' && data.coordinatorCert.length > 0 ? data.coordinatorCert : false;
    const acceptedWorkCert = typeof(data.acceptedWorkCert) == 'string' && data.acceptedWorkCert.length > 0 ? data.acceptedWorkCert : false;
    const dateCert = typeof(data.dateCert) == 'string' && data.dateCert.length > 0 ? new Date(data.dateCert) : false;


    if(oCustomer && host && name && date && dateEnd && url && type){

        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);

        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        let oCongress = {
            name,
            date,
            dateEnd,
            url,
            type,
            clientId: oClient.id
        }

        if(sCongressId){

            if(typeof(aPriceCateg) == 'object' && aPriceCateg instanceof Array && aPriceCateg.length > 0){
                const aProduct = [];
                for (let index = 0; index < aPriceCateg.length; index++) {
                    let oProduct = aPriceCateg[index];
                    if(oProduct){
                        oProduct.value = +aPriceCateg[index].value * 100;
                        aProduct.push(oProduct)
                    }
                }
              aPriceCateg = aProduct
            }

            oCongress = await Admin.app.models.Congress.findOne({where:{id:sCongressId}})
            oCongress.name = name ? name : oCongress.name;
            oCongress.date = date ? date : oCongress.date;
            oCongress.dateEnd = dateEnd ? dateEnd : oCongress.dateEnd;
            oCongress.type = type ? type : oCongress.type;
            oCongress.payment_methods = payment;
            oCongress.partnerFree = partnerFree;
            oCongress.products = aPriceCateg;
            oCongress.resume = resumeCongress;
            oCongress.topImageUrl = topImageUrl;
            oCongress.status = status;
            oCongress.subdivision = subdivision;
            oCongress.subdivs = subdivs;
            oCongress.categories = categories;
            oCongress.iesList = iesList;
            oCongress.cutNote = cutNote;
            oCongress.worksByCateg = worksByCateg;
            oCongress.certImageUrl = certImageUrl;
            oCongress.congressmanCert = congressmanCert;
            oCongress.avaliatorCert = avaliatorCert;
            oCongress.coordinatorCert = coordinatorCert;
            oCongress.acceptedWorkCert = acceptedWorkCert;
            oCongress.dateCert = dateCert;

            //oCongress['url'] = url ? url : oCongress.url;
            await oCongress.save()
        } else {
            // @TODO: Need to implement method to create URL.
            oCongress = await Admin.app.models.Congress.create(oCongress); 
        }

        // Super DEBUGGER 
        // let oActionTemplate = {
        //     date: new Date(),
        //     type: 'success',
        //     data: {
        //         message: 'Creating Congress',
        //         data: oCongress
        //     },
        //     customerId: oCustomer.id,
        //     clientId: oClient.id
        // }

        // if(!oProduct){
        //     oActionTemplate.type = 'danger';
        // }
        
        // Admin.app.models.ActionHistory.create(oActionTemplate);
        
        return oCongress; 
    }


    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
};

/**
 * this method list all congresses from institution
 * @param {string} host Host from institution
 * @param {Function(Error, object)} callback
 */
Admin.listCongress = async function(options, host, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;

    if(oCustomer && host){
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
       
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const aCongress = await Admin.app.models.Congress.find({
            where:{
                clientId: oClient.id
            },
            include:[
                'client',
                { "inscriptions" : ['customer', 'transaction'] },
                { "works" : [ {"avaliations" : ['customer'] } ] },
                { "cordinators" : ['customer'] },
                { "avaliators" : ['customer'] },
                { "books" : ['customer'] }
            ]
        });
        return aCongress;
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err);;
};

/**
 * Delete any register by referenceId and referenceModel
 * @param {string} host The host from application
 * @param {string} referenceId The reference Id to identify the registry
 * @param {string} referenceModel The reference model to identify what we are talking about here
 * @param {Function(Error, object)} callback
 */
Admin.delete = async function(options, host, referenceId, referenceModel, callback) {

    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;
    referenceId = typeof(referenceId) == 'string' && referenceId.length > 0 ? referenceId : false;
    referenceModel = typeof(referenceModel) == 'string' && referenceModel.length > 0 ? referenceModel : false;

    if(oCustomer && host && referenceId && referenceModel){

        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
       
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const oOperation = await Admin.app.models[referenceModel].findOne({where:{id:referenceId}});
        if(oOperation){
            return await oOperation.delete();
        }
    }
    

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err);

};

/**
 * This mehtod is used to delete the submition of some work
 * @param {string} host The host from application
 * @param {string} workId The work id
 * @param {Function(Error, object)} callback
 */
Admin.deleteWorkSubmition = async function(options, host, workId, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;
    workId = typeof(workId) == 'string' && workId.length > 0 ? workId : false;

    if(oCustomer && host && workId){

        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
       
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        let oWork = await Admin.app.models.Work.findById(workId);
        if(oWork){
            oWork.submitionWork = null;
            oWork.customerId = null;
            return await oWork.save();
        }
    }
    

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err);
};

/**
 * Upload File.
 */
Admin.uploadFile = async function(options, data) {
    const oCustomer = await options.accessToken.getClient();

    // const qr = new QrcodeDecoder();
    // WORK WITH IMAGE
    if(oCustomer){
        const form = new multiparty.Form();
        let file = await new Promise((resolve, reject) => {
            form.parse(data, (err, fields, files) => {
                const file = files['file'][0]; 
                if (!file) 
                    reject('File was not found in form data.');
                else
                    resolve(file.path)
            }) 
        })
        const uploader = await new _aws(file).uploadFile();
        return uploader;
    }

    return false;
} 

/**
 * This method helps to change the avaliator status first but we could expand for more
 * @param {object} data The objet must contain the avaliator reference and the data to update
 * @param {Function(Error, object)} callback
 */
Admin.updateAvaliator = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const avaliatorId = typeof(data.avaliatorId) == 'string' && data.avaliatorId.length > 0 ? data.avaliatorId : false;
    const status = typeof(data.status) == 'string' && data.status.length > 0 ? data.status : false;
    const ies = typeof(data.ies) == 'string' && data.ies.length > 0 ? data.ies : false;
    const categs = typeof(data.categs) == 'object' && data.categs instanceof Array && data.categs.length > 0 ? data.categs : false;



    if(oCustomer && host && congressId && avaliatorId){
        
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);

        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const oCongress = await Admin.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});

        if(!oCongress){
            var err = new Error("Missing required congress");
            err.name = "You cannot request that path Congress not found.";
            err.statusCode = 401;
            return callback(err)
        }

        const oAvaliator = await Admin.app.models.Avaliator.findOne({where:{and:[{_id:avaliatorId},{congressId:congressId}]}})

        if(!oAvaliator){
            var err = new Error("Missing required avaliator");
            err.name = "You cannot request that path Avaliator not found.";
            err.statusCode = 401;
            return callback(err)
        }

        if(status){
            oAvaliator.status = status;
        }

        if(ies){
            oAvaliator.ies = ies;
        }

        let finalObj = {};

        if(categs){
            for (let index = 0; index < categs.length; index++) {
                const category = categs[index];
                if(typeof(oAvaliator[category]) == 'undefined'){
                    oAvaliator[category] = true;
                }
            }

            for (let index = 0; index < categs.length; index++) {
                const category = categs[index];
                for (const key in oAvaliator) {
                    if (Object.hasOwnProperty.call(oAvaliator, key)) {
                      if(key.indexOf(category) > -1){
                        finalObj[key] = true;
                      }
                    }
                }
            }

            for (let index = 0; index < categs.length; index++) {
                const category = categs[index];
                for (const key in oAvaliator) {
                    if (Object.hasOwnProperty.call(oAvaliator, key)) {
                      if(key.indexOf(category) > -1){
                        oAvaliator[key] = true;
                      } else if(key.split(' ').length > 2 && !finalObj[key]){
                        oAvaliator[key] = false;  
                      }
                    }
                }
            }
        }

        return await oAvaliator.save();
    }


    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
}
  
/**
 * This method must set avaliator to some work by they respective ids
 * @param {object} data The data information to correctly register some work to avaliator
 * @param {Function(Error, boolean)} callback
 */
Admin.setAvaliatorToWork = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const avaliatorId1 = typeof(data.avaliatorId1) == 'string' && data.avaliatorId1.length > 0 ? data.avaliatorId1 : false;
    const avaliatorId2 = typeof(data.avaliatorId2) == 'string' && data.avaliatorId2.length > 0 ? data.avaliatorId2 : false;
    const avaliatorId3 = typeof(data.avaliatorId3) == 'string' && data.avaliatorId3.length > 0 ? data.avaliatorId3 : false;
    const workId = typeof(data.workId) == 'string' && data.workId.length > 0 ? data.workId : false;

    if(oCustomer && host && congressId && avaliatorId1 && workId){
       
        let filter = {where:{or:[{host},{front:host}]}}

        const oClient = await Admin.app.models.Client.findOne(filter);
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const oCongress = await Admin.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
        if(!oCongress){
            var err = new Error("Missing required congress");
            err.name = "You cannot request that path Congress not found.";
            err.statusCode = 401;
            return callback(err)
        }

        const oCordinator = await Admin.app.models.Cordinator.findOne({where:{and:[{congressId:oCongress.id},{customerId:oCustomer.id}]}});
        if(!oCordinator){
          var err = new Error("Missing required cordinator");
          err.name = "You cannot request that path cordinator not found.";
          err.statusCode = 401;
          return callback(err)
        }   

        if(oCustomer.isAdmin || oCordinator || oClient.team.indexOf(oCustomer.uid) > -1){

            const oAvaliator = await Admin.app.models.Avaliator.findOne({where:{and:[{_id:avaliatorId1},{congressId:congressId}]}})

            if(!oAvaliator){
                var err = new Error("Missing required avaliator");
                err.name = "You cannot request that path Avaliator not found.";
                err.statusCode = 401;
                return callback(err)
            }

            const oWork = await Admin.app.models.Work.findOne({where:{and:[{_id:workId},{congressId:congressId}]}})

            if(!oWork){
                var err = new Error("Missing required work");
                err.name = "You cannot request that path work not found.";
                err.statusCode = 401;
                return callback(err)
            }
            
            if(typeof(oWork.avaliators) != 'object'){
                oWork.avaliators = [];
            } 
            
            var hasAvaliator = false;
            for (let index = 0; index < oWork.avaliators.length; index++) {
                const oWorkAvaliator = oWork.avaliators[index];
                
                if(typeof(oWorkAvaliator) == 'object' && oWorkAvaliator){
                    if(oWorkAvaliator.toString() == oAvaliator.customerId.toString()){
                        hasAvaliator = true;
                    }
                }
                
            }

            if(!hasAvaliator){
                oWork.avaliators.push(oAvaliator.customerId.toString());
            }

            const oAvaliatorId2 = await Admin.app.models.Avaliator.findOne({where:{and:[{_id:avaliatorId2},{congressId:congressId}]}})

            if(oAvaliatorId2){
                hasAvaliator = false;
                
                for (let index = 0; index < oWork.avaliators.length; index++) {
                    const oWorkAvaliator = oWork.avaliators[index];
                    if(typeof(oWorkAvaliator) == 'object' && oWorkAvaliator){
                        if(oWorkAvaliator.toString() == oAvaliatorId2.customerId.toString()){
                            hasAvaliator = true;
                        }
                    }
                }

                if(!hasAvaliator){
                    oWork.avaliators.push(oAvaliatorId2.customerId.toString());
                }
            }

            const oAvaliatorId3 = await Admin.app.models.Avaliator.findOne({where:{and:[{_id:avaliatorId3},{congressId:congressId}]}})

            if(oAvaliatorId3){
                hasAvaliator = false;
                
                for (let index = 0; index < oWork.avaliators.length; index++) {
                    const oWorkAvaliator = oWork.avaliators[index];
                    if(typeof(oWorkAvaliator) == 'object' && oWorkAvaliator){
                        if(oWorkAvaliator.toString() == oAvaliatorId3.customerId.toString()){
                            hasAvaliator = true;
                        }
                    }
                }

                if(!hasAvaliator){
                    oWork.avaliators.push(oAvaliatorId3.customerId.toString());
                }
            }
        
            return await oWork.save();

        }

        
    }


    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
};

/**
 * This method must approve work and move the another congress
 * @param {object} data The data information to approve and move some work to another congress
 * @param {Function(Error, boolean)} callback
 */
Admin.approveAndMoveWork  = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const workId = typeof(data.workId) == 'string' && data.workId.length > 0 ? data.workId : false;

    if(oCustomer && host && congressId && workId){
       
        let filter = {where:{or:[{host},{front:host}]}}

        const oClient = await Admin.app.models.Client.findOne(filter);
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const oCongress = await Admin.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
        if(!oCongress){
            var err = new Error("Missing required congress");
            err.name = "You cannot request that path Congress not found.";
            err.statusCode = 401;
            return callback(err)
        }

        const oWork = await Admin.app.models.Work.findById(workId)
        if(!oWork){
            var err = new Error("Missing required work");
            err.name = "You cannot request that path work not found.";
            err.statusCode = 401;
            return callback(err)
        }

        if(oCustomer.isAdmin | oClient.team.indexOf(oCustomer.uid) > -1){
            
            const oWorkTemplate = {
                workName : oWork.workName,
                congressId : oCongress.id,
                oldCongressId: oWork.congressId,
                host : oWork.host,
                ies : oWork.ies,
                category : oWork.category,
                leaderName : oWork.leaderName,
                leaderDoc : oWork.leaderDoc,
                date : new Date(),
                customerId : oWork.customerId,
                type: oWork.type,
                submitionWork: {
                    coAuthors: oWork.submitionWork.coAuthors,
                    date: oWork.submitionWork.date,
                    discipline: oWork.submitionWork.discipline,
                    lectiveYear: oWork.submitionWork.lectiveYear,
                    linkCloudWork: oWork.submitionWork.linkCloudWork,
                    productionDescription: oWork.submitionWork.productionDescription,
                    researchRealized: oWork.submitionWork.researchRealized,
                    responsableTeatcher: oWork.submitionWork.responsableTeatcher,
                    resumeWork: oWork.submitionWork.resumeWork,
                    studyObject: oWork.submitionWork.studyObject,
                    oldWorkId: oWork.submitionWork.workId,
                    workOf: oWork.submitionWork.workOf
                }
            }
            const oCreatedWork = await Admin.app.models.Work.create(oWorkTemplate)
            oCreatedWork.submitionWork.workId = oCreatedWork.id;
            await oCreatedWork.save();
            return oCreatedWork;
        }
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
};

/**
 * This method must recieve data from client, congress, categories also the customerId to create the correct cordinator
 * @param {object} data The data contain all the information
 * @param {Function(Error, boolean)} callback
 */
Admin.setCordinator = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const categs = typeof(data.categs) == 'object' && data.categs.length > 0 ? data.categs : [];
    const customers = _utl.inputCheckboxToArray(data,'cordinator');

    if(oCustomer && host && congressId && categs && customers){
        
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);

        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const oCongress = await Admin.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});

        if(!oCongress){
            var err = new Error("Missing required congress");
            err.name = "You cannot request that path Congress not found.";
            err.statusCode = 401;
            return callback(err)
        }
    
        
        if(typeof(customers) == 'object' && customers instanceof Array && customers.length > 0){
            let response = [];
            for (let index = 0; index < customers.length; index++) {
                const cordinatorTemplate = {
                    categs,
                    host,
                    congressId,
                    clientId: oClient.id,
                    customerId: customers[index]
                }

                let cordinator = await Admin.app.models.Cordinator.findOne({where:{customerId:customers[index],congressId}})
                if(!cordinator){
                    cordinator = await Admin.app.models.Cordinator.create(cordinatorTemplate);
                } else {
                    cordinator.categs = cordinatorTemplate.categs
                    cordinator.host = cordinatorTemplate.host
                    cordinator.congressId = cordinatorTemplate.congressId
                    cordinator.clientId = cordinatorTemplate.clientId
                    cordinator.customerId = cordinatorTemplate.customerId
                    await cordinator.save();
                }

                response.push(cordinator)
            }
            return response;
        }
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
};

/**
 * This method updates information about hte indications of some work and the work itself
 * @param {object} data The information contain the data of work indication
 * @param {Function(Error, object)} callback
 */
Admin.updateWorkDetails = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const workId = typeof(data.workId) == 'string' && data.workId.length > 0 ? data.workId : false;

    if(oCustomer && host && congressId && workId){
        
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);

        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const oCongress = await Admin.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});

        if(!oCongress){
            var err = new Error("Missing required congress");
            err.name = "You cannot request that path Congress not found.";
            err.statusCode = 401;
            return callback(err)
        }

        const oWork = await Admin.app.models.Work.findOne({where:{and:[{_id:workId},{congressId: oCongress.id}]}});

        if(!oWork){
            var err = new Error("Missing required work");
            err.name = "You cannot request that path Work not found.";
            err.statusCode = 401;
            return callback(err)
        }
   
        oWork.workName = typeof(data.workName) == 'string' && data.workName != oWork.workName && data.workName.length > 0 ? data.workName : oWork.workName; 
        oWork.name = typeof(data.name) == 'string' && data.name != oWork.name && data.name.length > 0 ? data.name : oWork.name; 
        oWork.roleies = typeof(data.roleies) == 'string' && data.roleies != oWork.roleies && data.roleies.length > 0 ? data.roleies : oWork.roleies; 
        oWork.cpf = typeof(data.cpf) == 'string' && data.cpf != oWork.cpf && data.cpf.length > 0 ? data.cpf : oWork.cpf; 
        oWork.mobile = typeof(data.mobile) == 'string' && data.mobile != oWork.mobile && data.mobile.length > 0 ? data.mobile : oWork.mobile; 
        oWork.email = typeof(data.email) == 'string' && data.email != oWork.email && data.email.length > 0 ? data.email : oWork.email; 
        oWork.leaderName = typeof(data.leaderName) == 'string' && data.leaderName != oWork.leaderName && data.leaderName.length > 0 ? data.leaderName : oWork.leaderName; 
        oWork.leaderDoc = typeof(data.leaderDoc) == 'string' && data.leaderDoc != oWork.leaderDoc && data.leaderDoc.length > 0 ? data.leaderDoc : oWork.leaderDoc; 
        oWork.ies = typeof(data.ies) == 'string' && data.ies != oWork.ies && data.ies.length > 0 ? data.ies : oWork.ies; 
        oWork.category = typeof(data.category) == 'string' && data.category != oWork.category && data.category.length > 0 ? data.category : oWork.category; 

        return await oWork.save();
  
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
};

/**
 * This methods brings thew power to admins could manage isentation to some user
 * @param {object} data All data information from request
 * @param {Function(Error, object)} callback
 */
Admin.createCustomerIsentation = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const productId = typeof(data.productId) == 'string' && data.productId.length > 0 ? data.productId : false;
    const customerId = typeof(data.customerId) == 'string' && data.customerId.length > 0 ? data.customerId : false;
    const productTypeIsentation = typeof(data.productTypeIsentation) == 'string' && data.productTypeIsentation.length > 0 ? data.productTypeIsentation : false
    
    if(oCustomer && host && customerId && productTypeIsentation){
        
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const response = await Admin[productTypeIsentation](productId, customerId, oClient.id, oCustomer.id, callback);
        return response;

    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)

};

Admin.membership = async function(producId, customerId, clientId, ownerId, callback){
    const oCustomer = await Admin.app.models.Customer.findById(customerId);
    if(oCustomer && producId && clientId && ownerId){
        const oClient = await Admin.app.models.Client.findById(clientId);
        if(oClient){
            const oProduct = await Admin.app.models.Product.findById(producId)
            if(oProduct){
                const amount = oProduct.price;
                
                const oTransactionTemplate = {
                    amount: amount,
                    status: "exempt",
                    processing: "done",
                    date: new Date(),
                    ref: `Pagamento: ${oProduct.name}`,
                    link: 'membership',
                    type: "platform_included",
                    manager: ownerId,
                    clientId: oClient.id,
                    tx: { items: [oProduct] }
                };
                
                const oInscription = await oCustomer.transactions.create(oTransactionTemplate);
                
                return oInscription;
            }
        }
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
}

Admin.inscription = async function(producId, customerId, clientId, ownerId, callback){

    const oCustomer = await Admin.app.models.Customer.findById(customerId);

    if(oCustomer && producId && clientId && ownerId){
        const oClient = await Admin.app.models.Client.findById(clientId);

        if(oClient){
              
            const oCongress = await Admin.app.models.Congress.findById(producId);

            if(oCongress){

                const oInscriptionTemplate = {
                    date: new Date(),
                    type: `exempt`,
                    congressId: oCongress.id,
                    oPrice: {
                        label: `Isenção: ${oCongress.name}`,
                        valie: "0"
                    },
                    link: 'inscription',
                    manager: ownerId,
                }

                const oInscription = await oCustomer.inscriptions.create(oInscriptionTemplate);

                return oInscription

            }
        }
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)

}

/**
 * This method will use to create payment for user, we can create pix or boleto
 * @param {object} data The information about the payment to create
 * @param {Function(Error, object)} callback
 */
Admin.createCustomerPayment = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const productId = typeof(data.productId) == 'string' && data.productId.length > 0 ? data.productId : false;
    const customerId = typeof(data.customerId) == 'string' && data.customerId.length > 0 ? data.customerId : false;
    const productTypeIsentation = typeof(data.productTypeIsentation) == 'string' && data.productTypeIsentation.length > 0 ? data.productTypeIsentation : false;
    const amountAdm = typeof(data.amountAdm) == 'string' && data.amountAdm.length > 0 ? data.amountAdm : false;
    const token = typeof(data.token) == 'string' && data.token.length > 0 ? data.token : false;
    
    if(oCustomer && host && customerId && productTypeIsentation && amountAdm){
        
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const response = await Admin[`${productTypeIsentation}CP`](productId, customerId, oClient.id, oCustomer.id, token, +amountAdm, callback);
        return response;

    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
};

Admin.membershipCP = async function(producId, customerId, clientId, ownerId, token, amount, callback){
    const oCapturedTransaction = await new pagarme(Admin.app.dataSources.pagarmeDs).capture({
        token,
        amount
    });

    const oCustomer = await Admin.app.models.Customer.findById(customerId);
    if(oCustomer && producId && clientId && ownerId){
        const oClient = await Admin.app.models.Client.findById(clientId);
        if(oClient){
            const oProduct = await Admin.app.models.Product.findById(producId)
            if(oProduct){
                const amount = oProduct.price;
                
                const oTransactionTemplate = {
                    amount: oCapturedTransaction.paid_amount,
                    status: oCapturedTransaction.status,
                    processing: oCapturedTransaction.status == 'waiting_payment' ? 'waiting' : 'done',
                    date: new Date(),
                    ref: `Pagamento: ${oProduct.name}`,
                    link: 'membership',
                    type: "platform_include",
                    madeBy: ownerId,
                    clientId: oClient.id,
                    tx: {
                        items: [
                            {
                                object : "item",
                                id : oProduct.id,
                                title : `Pagamento: ${oProduct.name}`,
                                unit_price : oCapturedTransaction.amount,
                                quantity : 1,
                                category : null,
                                tangible : false,
                                venue : null,
                                date : new Date()
                            }
                        ]
                    }
                };
                
                const oInscription = await oCustomer.transactions.create(oTransactionTemplate);
                
                return {...oInscription, ...oCapturedTransaction}
            }
        }
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
}

Admin.inscriptionCP = async function(producId, customerId, clientId, ownerId, token, amount, callback){
    const oCapturedTransaction = await new pagarme(Admin.app.dataSources.pagarmeDs).capture({
        token,
        amount
    });

    const oCustomer = await Admin.app.models.Customer.findById(customerId);

    if(oCustomer && producId && clientId && ownerId){
        const oClient = await Admin.app.models.Client.findById(clientId);
        if(oClient){
              
            const oCongress = await Admin.app.models.Congress.findById(producId);

            if(oCongress){

                if(oCapturedTransaction){

                    const oTransactionTemplate = {
                        amount: oCapturedTransaction.paid_amount,
                        status: oCapturedTransaction.status,
                        processing: oCapturedTransaction.status == 'waiting_payment' ? 'waiting' : 'done',
                        date: new Date(),
                        ref: `Pagamento Congresso: ${oCongress.name}`,
                        link: 'inscription',
                        type: "platform_include",
                        madeBy: ownerId,
                        clientId: oClient.id,
                        tx: {
                            items: [
                                {
                                    object : "item",
                                    id : oCongress.id,
                                    title : `Pagamento Congresso: ${oCongress.name}`,
                                    unit_price : oCapturedTransaction.amount,
                                    quantity : 1,
                                    category : null,
                                    tangible : false,
                                    venue : null,
                                    date : new Date()
                                }
                            ]
                        }
                    };
                    
                    const oTx = await oCustomer.transactions.create(oTransactionTemplate);
            
                    const oInscriptionTemplate = {
                        date: new Date(),
                        type: `inscription`,
                        congressId: oCongress.id,
                        transactionId: oTx.id,
                        oPrice: {value: oCapturedTransaction.amount, label: `Pagamento Congresso: ${oCongress.name}`}
                    }
            
                    const oInscription = await oCustomer.inscriptions.create(oInscriptionTemplate);

                    return {...oInscription, ...oTx, ...oCapturedTransaction}

                }

            }
        }
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
}

/**
 * This method allows to admin update the user data information
 * @param {object} data Data from the customer
 * @param {Function(Error, object)} callback
 */
Admin.updateCustomerDataInformation = async function(options, data, callback) {
    
    const oRequester = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const userId = typeof(data.userId) == 'string' && data.userId.length > 0 ? data.userId : false;
    
    if(oRequester && host && userId){
        
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oRequester.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oRequester.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }


        const oCustomer = await Admin.app.models.Customer.findById(userId); 

        if(oCustomer){
            madeBy: oRequester.id,
            oCustomer.name = data.name ? data.name : oCustomer.name ? oCustomer.name : undefined;
            oCustomer.name = data.name ? data.name : oCustomer.name ? oCustomer.name : undefined;
            oCustomer.gender = data.gender ? data.gender : oCustomer.gender ? oCustomer.gender : undefined;
            oCustomer.birthday = data.birthday ? data.birthday : oCustomer.birthday ? oCustomer.birthday : undefined;
            oCustomer.cpf = data.cpf ? data.cpf : oCustomer.cpf ? oCustomer.cpf : undefined;
            oCustomer.rg = data.rg ? data.rg : oCustomer.rg ? oCustomer.rg : undefined;
            oCustomer.phone = data.phone ? data.phone : oCustomer.phone ? oCustomer.phone : undefined;
            oCustomer.mobile = data.mobile ? data.mobile : oCustomer.mobile ? oCustomer.mobile : undefined;
            oCustomer.email = data.email ? data.email : oCustomer.email ? oCustomer.email : undefined;
            oCustomer.address = data.address ? data.address : oCustomer.address ? oCustomer.address : undefined;
            oCustomer.address_number = data.address_number ? data.address_number : oCustomer.address_number ? oCustomer.address_number : undefined;
            oCustomer.address_complement = data.address_complement ? data.address_complement : oCustomer.address_complement ? oCustomer.address_complement : undefined;
            oCustomer.address_neighborhood = data.address_neighborhood ? data.address_neighborhood : oCustomer.address_neighborhood ? oCustomer.address_neighborhood : undefined;
            oCustomer.address_city = data.address_city ? data.address_city : oCustomer.address_city ? oCustomer.address_city : undefined;
            oCustomer.address_state = data.address_state ? data.address_state : oCustomer.address_state ? oCustomer.address_state : undefined;
            oCustomer.address_country = data.address_country ? data.address_country : oCustomer.address_country ? oCustomer.address_country : undefined;
            oCustomer.postal_code = data.postal_code ? data.postal_code : oCustomer.postal_code ? oCustomer.postal_code : undefined;
            oCustomer.role = data.role ? data.role : oCustomer.role ? oCustomer.role : undefined;
            oCustomer.institution_abreviation = data.institution_abreviation ? data.institution_abreviation : oCustomer.institution_abreviation ? oCustomer.institution_abreviation : undefined;
            oCustomer.professional_phone = data.professional_phone ? data.professional_phone : oCustomer.professional_phone ? oCustomer.professional_phone : undefined;
            oCustomer.professional_address = data.professional_address ? data.professional_address : oCustomer.professional_address ? oCustomer.professional_address : undefined;
            oCustomer.professional_address_complement = data.professional_address_complement ? data.professional_address_complement : oCustomer.professional_address_complement ? oCustomer.professional_address_complement : undefined;
            oCustomer.professional_address_neighborhood = data.professional_address_neighborhood ? data.professional_address_neighborhood : oCustomer.professional_address_neighborhood ? oCustomer.professional_address_neighborhood : undefined;
            oCustomer.professional_address_city = data.professional_address_city ? data.professional_address_city : oCustomer.professional_address_city ? oCustomer.professional_address_city : undefined;
            oCustomer.professional_address_state = data.professional_address_state ? data.professional_address_state : oCustomer.professional_address_state ? oCustomer.professional_address_state : undefined;
            oCustomer.professional_address_country = data.professional_address_country ? data.professional_address_country : oCustomer.professional_address_country ? oCustomer.professional_address_country : undefined;
            oCustomer.professional_postal_code = data.professional_postal_code ? data.professional_postal_code : oCustomer.professional_postal_code ? oCustomer.professional_postal_code : undefined;
            oCustomer.title = data.title ? data.title : oCustomer.title ? oCustomer.title : undefined;
            oCustomer.degree_institution = data.degree_institution ? data.degree_institution : oCustomer.degree_institution ? oCustomer.degree_institution : undefined;
            oCustomer.degree_course = data.degree_course ? data.degree_course : oCustomer.degree_course ? oCustomer.degree_course : undefined;
            oCustomer.degree_city = data.degree_city ? data.degree_city : oCustomer.degree_city ? oCustomer.degree_city : undefined;
            oCustomer.year_conclusion_degree = data.year_conclusion_degree ? data.year_conclusion_degree : oCustomer.year_conclusion_degree ? oCustomer.year_conclusion_degree : undefined;
            oCustomer.specialization_institution  = data.specialization_institution ? data.specialization_institution : oCustomer.specialization_institution ? oCustomer.specialization_institution : undefined;
            oCustomer.specialization_course  = data.specialization_course ? data.specialization_course : oCustomer.specialization_course ? oCustomer.specialization_course : undefined;
            oCustomer.year_conclusion_specialization  = data.year_conclusion_specialization ? data.year_conclusion_specialization : oCustomer.year_conclusion_specialization ? oCustomer.year_conclusion_specialization : undefined;
            oCustomer.master_institution  = data.master_institution ? data.master_institution : oCustomer.master_institution ? oCustomer.master_institution : undefined;
            oCustomer.master_course  = data.master_course ? data.master_course : oCustomer.master_course ? oCustomer.master_course : undefined;
            oCustomer.year_conclusion_master  = data.year_conclusion_master ? data.year_conclusion_master : oCustomer.year_conclusion_master ? oCustomer.year_conclusion_master : undefined;
            oCustomer.doc_institution  = data.doc_institution ? data.doc_institution : oCustomer.doc_institution ? oCustomer.doc_institution : undefined;
            oCustomer.doc_course  = data.doc_course ? data.doc_course : oCustomer.doc_course ? oCustomer.doc_course : undefined;
            oCustomer.year_conclusion_doc  = data.year_conclusion_doc ? data.year_conclusion_doc : oCustomer.year_conclusion_doc ? oCustomer.year_conclusion_doc : undefined;
            oCustomer.phd_institution  = data.phd_institution ? data.phd_institution : oCustomer.phd_institution ? oCustomer.phd_institution : undefined;
            oCustomer.phd_course  = data.phd_course ? data.phd_course : oCustomer.phd_course ? oCustomer.phd_course : undefined;
            oCustomer.year_conclusion_phd  = data.year_conclusion_phd ? data.year_conclusion_phd : oCustomer.year_conclusion_phd ? oCustomer.year_conclusion_phd : undefined;
        
            return await oCustomer.save();
        }
    
        var err = new Error("Missing required level");
        err.name = "You cannot request that path.";
        err.statusCode = 401;
        return callback(err)


    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)


    



};

/**
 * This method must remove the avaliator from work
 * @param {object} data All information about the host, avaliator and work
 * @param {Function(Error, object)} callback
 */
Admin.removeAvaliatorFromWork = async function(options, host, workId, avaliatorId, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;
    workId = typeof(workId) == 'string' && workId.length > 0 ? workId : false;
    avaliatorId = typeof(avaliatorId) == 'string' && avaliatorId.length > 0 ? avaliatorId : false;

    if(oCustomer && host && workId && avaliatorId){

        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
       
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        let oWork = await Admin.app.models.Work.findById(workId);
        if(oWork && typeof(oWork.avaliators) == 'object' && oWork.avaliators instanceof Array && oWork.avaliators.length > 0 ){
            for (let index = 0; index < oWork.avaliators.length; index++) {
                let avaliator = oWork.avaliators[index];
                    avaliator = avaliator.toString();
                if(avaliator == avaliatorId){
                    delete oWork.avaliators[index];
                    
                }
            }
            return await oWork.save();
        }
    }
    

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err);
};

/**
 * This method helps to change the publicom status first but we could expand for more
 * @param {object} data The objet must contain the publicom reference and the data to update
 * @param {Function(Error, object)} callback
 */
Admin.changePublicomStatus = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const bookId = typeof(data.bookId) == 'string' && data.bookId.length > 0 ? data.bookId : false;
    const status = typeof(data.status) == 'string' && data.status.length > 0 ? data.status : false;

    if(oCustomer && host && congressId && bookId){
        
        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);

        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const oCongress = await Admin.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});

        if(!oCongress){
            var err = new Error("Missing required congress");
            err.name = "You cannot request that path Congress not found.";
            err.statusCode = 401;
            return callback(err)
        }

        const oBook = await Admin.app.models.Book.findOne({where:{and:[{id:bookId},{congressId:oCongress.id},{clientId:oClient.id}]}});
        if(!oBook){
            var err = new Error("Missing required book");
            err.name = "You cannot request that path Avaliator not found.";
            err.statusCode = 401;
            return callback(err)
        }

        if(status){
            oBook.status = status;
        }

        return await oBook.save();
    }


    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
}

/**
 * This mehtod is used to delete the submition of somebook 
 * @param {string} host The host from application
 * @param {string} bookId The book id
 * @param {Function(Error, object)} callback
 */
Admin.deletePublicomSubmition = async function(options, host, congressId, bookId, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;
    congressId = typeof(congressId) == 'string' && congressId.length > 0 ? congressId : false;
    bookId = typeof(bookId) == 'string' && bookId.length > 0 ? bookId : false;

    if(oCustomer && host && congressId && bookId){

        let filter = {
            where:{
                and:[
                    {
                        team:{
                            inq: [oCustomer.uid]
                        }
                    },
                    {host: host}
                ]
            }
        }

        if(oCustomer.isAdmin){
            filter = {where:{host}}
        }

        const oClient = await Admin.app.models.Client.findOne(filter);
       
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        let oBook = await Admin.app.models.Book.findOne({where:{and:[{id:bookId},{congressId},{clientId:oClient.id}]}});
        if(oBook){
            return await oBook.delete();
        }
    }
    

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err);
};

};