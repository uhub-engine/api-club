'use strict';

const utils = require('../../server/helpers/utils');
const _frb = require('../../server/services/firebase');
const pagarme = require('../../server/services/pagarme');
const sengrid = require('../../server/services/sendgrid');
const multiparty = require('multiparty');
const _aws = require('../../server/services/aws.js');

module.exports = function(Authenticated) {

  /**
   * This method helps  the user to authenticate using access token from firebase source uid to check the integrity;
   * @param {object} data Data from request
   * @param {Function(Error, object)} callback
   */
  Authenticated.authenticateUser = async function(data, callback) {
    const oResponse = await _frb.verifyIdToken(data.accessToken);
    const front = typeof(data.front) == 'string' && data.front.length > 0 ? data.front : false;
    if(oResponse && front){
      const oClient = await Authenticated.app.models.Client.findOne({where:{front}});

      if(!oClient){
        var err = new Error("Missing required institution");
        err.name = "You cannot request that path, institution does not exist.";
        err.statusCode = 403;
        return callback(err)
      }

      let oUser = await Authenticated.app.models.Customer.findOne({where:{or:[{uid:data.uid},{email:data.email}]}});
      
      if(!oUser){
        oUser = await Authenticated.app.models.Customer.create({
            uid: data.uid,
            username: data.uid,
            password: data.uid,
            email: data.email || `${data.uid}@uhub.app`,
            name: data.name || null,
            picture: data.picture || null
        });
      }

      if(typeof(oUser.uid) == 'undefined' && typeof(oUser.email) == 'string' && oUser.email.length > 0){
        oUser.uid = data.uid,
        oUser.username = data.uid;
        oUser.name = data.name || null;
        oUser.picture = data.picture || null;
        await oUser.save();
      }

      if(typeof(oUser.passport) == 'object' && oUser.passport instanceof Array){
        var clientId = oClient.id.toString();
        var hasOnPassport = await oUser.passport.filter(oClientId => oClientId.toString() == clientId);
        if(hasOnPassport.length < 1){
          oUser.passport.push(oClient.id);
          await oUser.save();
        }
      } else {
        oUser.passport = [oClient.id];
        await oUser.save();
      }



      //Check has active session
      let oAuthenticatedUser = await oUser.getClientByTokenId(data.accessToken);
      if(!oAuthenticatedUser){
          oAuthenticatedUser = await oUser.createClientAccessTokenByTokenId(data);
      }
      return oAuthenticatedUser;
    }
  };

  /**
   * This method verify if costumer has pay the institution into corrent date.
   * @param {string} host The front application host to check member status
   * @param {Function(Error, object)} callback
   */
  Authenticated.checkAssociationStatus = async function(options, host, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false   

    if(oCustomer && host){

      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});

      if(oClient){

        const oProduct = await Authenticated.app.models.Product.findOne({where:{and:[{default:true},{clientId:oClient.id}]}});

        const oTransaction = await Authenticated.app.models.Transaction.findOne(
          {
            where:{
              and:[
                {customerId:oCustomer.id},
                {clientId: oClient.id},
                {status:'paid'},
                {amount: oProduct.price},
                {date: {gt: new Date(oProduct.date)}}
              ]
            },
            order: 'date DESC'
          });

        
        var today = new Date();
        var response = {
          name: `Anuidade ${today.getFullYear()}`,
          active: false
        };


        if(oProduct && oTransaction){

          
          var from = new Date(oProduct.date);  // -1 because months are from 0 to 11
          var year = from.getFullYear();
          var month = from.getMonth();
          var day = from.getDate();
          var to = new Date(year + 1, 11, 31);
          var check = new Date(oTransaction.date);

          // console.log(from.getFullYear(), to.getFullYear(), check.getFullYear())

          if(check.getFullYear() >= from.getFullYear() && check < to){
            response.active = true;
          }

        }


        return response

      }

    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * This method gets the product list to became member.
   * @param {string} host The institution front host
   * @param {Function(Error, object)} callback
   */
  Authenticated.getInstitutionActiveProduct = async function(options, host, callback) {
    const oCustomer = await options.accessToken.getClient(); 

    if(oCustomer){
      host = typeof(host) == 'string' && host.length > 0 ? host : false;
      let filter = { where:{ front: host } }
      const oClient = await Authenticated.app.models.Client.findOne(filter);      
      const aTransactions = await oCustomer.transactions.find({where:{link:'membership'}});

      if(host && oClient){
        
        if(aTransactions.length > 0){
        
          const oDateLastPayment = new Date(Math.max(...aTransactions.map(e => new Date(e.date))));
          const aProducts = await Authenticated.app.models.Product.find({where:{and:[{clientId:oClient.id},{date:{gt:oDateLastPayment}}]}});
          for (let index = 0; index < aProducts.length; index++) {
            const oProduct = aProducts[index];
            const checkDefault = typeof(oProduct.default) == 'boolean' ? oProduct.default : false;
            const checkAmnesty = typeof(oProduct.amnesty) == 'boolean' ? oProduct.amnesty : false;
            if(checkDefault && checkAmnesty){
              return [oProduct];
            }
          }
          return aProducts;
        } else {
          let oProduct = await Authenticated.app.models.Product.findOne({where:{and:[{default:true},{clientId:oClient.id}]}});
          if(oProduct){
            return [oProduct];
          }
        }

        var err = new Error("Not found active product");
        err.name = `Not found any avaiable product`;
        err.statusCode = 404;
        return callback(err)
        
      }

      var err = new Error("Missing required fields");
      err.name = `You cannot request that path: ${host}.`;
      err.statusCode = 400;
      return callback(err)

    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * This method receives token to capture the payment trough gateway provider
   * @param {object} data Information about the transaction and host
   * @param {Function(Error, object)} callback
   */
  Authenticated.processMembership = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const token = typeof(data.token) == 'string' && data.token.length > 0 ? data.token : false;
    
    if(oCustomer && host && token){
      const oClient = await Authenticated.app.models.Client.findOne({where:{front: host}})
      const aTransactions = await oCustomer.transactions.find({where:{link:'membership'}});
      if(oClient){
        
        if(aTransactions.length > 0){
          let sProductName = '';
          let amount = 0;
          const oDateLastPayment = new Date(Math.max(...aTransactions.map(e => new Date(e.date))));
          const aProducts = await Authenticated.app.models.Product.find({where:{and:[{clientId:oClient.id},{date:{gt:oDateLastPayment}}]}});
          if(aProducts.length > 0){
            for (let index = 0; index < aProducts.length; index++) {
              const oProduct = aProducts[index];
              const checkDefault = typeof(oProduct.default) == 'boolean' ? oProduct.default : false;
              const checkAmnesty = typeof(oProduct.amnesty) == 'boolean' ? oProduct.amnesty : false;
              if(checkDefault && checkAmnesty){
                sProductName = oProduct.name;
                amount = oProduct.price;
                break;
              } else {
                sProductName += `${oProduct.name},`
                amount += oProduct.price;
              }
            }
            
            const oCapturedTransaction = await new pagarme(Authenticated.app.dataSources.pagarmeDs).capture({
              token,
              amount
            });
    
            const oTransactionTemplate = {
              amount: oCapturedTransaction.paid_amount,
              status: oCapturedTransaction.status,
              processing: oCapturedTransaction.status == 'waiting_payment' ? 'waiting' : 'done',
              date: new Date(),
              ref: `Pagamento: ${sProductName}`,
              link: 'membership',
              type: oCapturedTransaction.payment_method,
              clientId: oClient.id,
              tx: oCapturedTransaction
            };
            
            oCustomer.transactions.create(oTransactionTemplate)
            
            return {
              status: oTransactionTemplate.status,
              date: oTransactionTemplate.date,
              type: oCapturedTransaction.payment_method,
              boleto_url: oCapturedTransaction.boleto_url || '',
              boleto_barcode: oCapturedTransaction.boleto_barcode || '',
              tx : oCapturedTransaction
            }
          } else {
            var err = new Error("Already has one payment waiting");
            err.name = "The user already has one transaction waiting payment on valid date.";
            err.statusCode = 403;
            return callback(err)
          }

        } else {
        
          const oProduct = await Authenticated.app.models.Product.findOne({where:{and:[{default:true},{clientId:oClient.id}]}});
          if(oProduct){
            const amount = oProduct.price;
            const oCapturedTransaction = await new pagarme(Authenticated.app.dataSources.pagarmeDs).capture({
              token,
              amount
            });

            const oTransactionTemplate = {
              amount: oCapturedTransaction.paid_amount,
              status: oCapturedTransaction.status,
              processing: oCapturedTransaction.status == 'waiting_payment' ? 'waiting' : 'done',
              date: new Date(),
              ref: `Pagamento: ${oProduct.name}`,
              link: 'membership',
              type: oCapturedTransaction.payment_method,
              clientId: oClient.id,
              tx: oCapturedTransaction
            };
            
            oCustomer.transactions.create(oTransactionTemplate)
            
            return {
              status: oTransactionTemplate.status,
              date: oTransactionTemplate.date,
              type: oCapturedTransaction.payment_method,
              boleto_url: oCapturedTransaction.boleto_url || '',
              boleto_barcode: oCapturedTransaction.boleto_barcode || ''
            };
          }

        }
        
      }
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * This method must list all the transactions into user institution
   * @param {string} host The application front host
   * @param {Function(Error, array)} callback
   */
  Authenticated.getUserTransactionsFromInstitution = async function(options, host, callback) {
    const oCustomer = await options.accessToken.getClient(); 

    if(oCustomer){
      host = typeof(host) == 'string' && host.length > 0 ? host : false;
      const filter = { where:{ front: host } }
      const oClient = await Authenticated.app.models.Client.findOne(filter);    

      if(oClient){
        const aCustomerTransactions = await Authenticated.app.models.Transaction.find({where:{and:[{customerId:oCustomer.id},{clientId:oClient.id}]},order: 'date DESC'});
        return aCustomerTransactions;
      } 

    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * This method allows the user to update data.
   * @param {object} data The data to update user information
   * @param {Function(Error, object)} callback
   */
  Authenticated.updateCustomerDetails = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 

    if(oCustomer){
      oCustomer.name = data.name ? data.name : oCustomer.name ? oCustomer.name : undefined;
      oCustomer.gender = data.gender ? data.gender : oCustomer.gender ? oCustomer.gender : undefined;
      oCustomer.maritalStatus = data.maritalStatus ? data.maritalStatus : oCustomer.maritalStatus ? oCustomer.maritalStatus : undefined;
      oCustomer.birthday = data.birthday ? data.birthday : oCustomer.birthday ? oCustomer.birthday : undefined;
      oCustomer.cpf = data.cpf ? data.cpf : oCustomer.cpf ? oCustomer.cpf : undefined;
      oCustomer.rg = data.rg ? data.rg : oCustomer.rg ? oCustomer.rg : undefined;
      oCustomer.passport_number = data.passport_number ? data.passport_number : oCustomer.passport_number ? oCustomer.passport_number : undefined;
      oCustomer.passportImageUrl = data.passportImageUrl ? data.passportImageUrl : oCustomer.passportImageUrl ? oCustomer.passportImageUrl : undefined;
      oCustomer.residenceImageUrl = data.residenceImageUrl ? data.residenceImageUrl : oCustomer.residenceImageUrl ? oCustomer.residenceImageUrl : undefined;
      oCustomer.signatureImageUrl = data.signatureImageUrl ? data.signatureImageUrl : oCustomer.signatureImageUrl ? oCustomer.signatureImageUrl : undefined;
      oCustomer.phone = data.phone ? data.phone : oCustomer.phone ? oCustomer.phone : undefined;
      oCustomer.mobile = data.mobile ? data.mobile : oCustomer.mobile ? oCustomer.mobile : undefined;
      oCustomer.email = data.email ? data.email : oCustomer.email ? oCustomer.email : undefined;
      oCustomer.address = data.address ? data.address : oCustomer.address ? oCustomer.address : undefined;
      oCustomer.address_number = data.address_number ? data.address_number : oCustomer.address_number ? oCustomer.address_number : undefined;
      oCustomer.address_complement = data.address_complement ? data.address_complement : oCustomer.address_complement ? oCustomer.address_complement : undefined;
      oCustomer.address_neighborhood = data.address_neighborhood ? data.address_neighborhood : oCustomer.address_neighborhood ? oCustomer.address_neighborhood : undefined;
      oCustomer.address_city = data.address_city ? data.address_city : oCustomer.address_city ? oCustomer.address_city : undefined;
      oCustomer.address_state = data.address_state ? data.address_state : oCustomer.address_state ? oCustomer.address_state : undefined;
      oCustomer.address_country = data.address_country ? data.address_country : oCustomer.address_country ? oCustomer.address_country : undefined;
      oCustomer.postal_code = data.postal_code ? data.postal_code : oCustomer.postal_code ? oCustomer.postal_code : undefined;
      oCustomer.role = data.role ? data.role : oCustomer.role ? oCustomer.role : undefined;
      oCustomer.institution_abreviation = data.institution_abreviation ? data.institution_abreviation : oCustomer.institution_abreviation ? oCustomer.institution_abreviation : undefined;
      oCustomer.professional_phone = data.professional_phone ? data.professional_phone : oCustomer.professional_phone ? oCustomer.professional_phone : undefined;
      oCustomer.professional_address = data.professional_address ? data.professional_address : oCustomer.professional_address ? oCustomer.professional_address : undefined;
      oCustomer.professional_address_complement = data.professional_address_complement ? data.professional_address_complement : oCustomer.professional_address_complement ? oCustomer.professional_address_complement : undefined;
      oCustomer.professional_address_neighborhood = data.professional_address_neighborhood ? data.professional_address_neighborhood : oCustomer.professional_address_neighborhood ? oCustomer.professional_address_neighborhood : undefined;
      oCustomer.professional_address_city = data.professional_address_city ? data.professional_address_city : oCustomer.professional_address_city ? oCustomer.professional_address_city : undefined;
      oCustomer.professional_address_state = data.professional_address_state ? data.professional_address_state : oCustomer.professional_address_state ? oCustomer.professional_address_state : undefined;
      oCustomer.professional_address_country = data.professional_address_country ? data.professional_address_country : oCustomer.professional_address_country ? oCustomer.professional_address_country : undefined;
      oCustomer.professional_postal_code = data.professional_postal_code ? data.professional_postal_code : oCustomer.professional_postal_code ? oCustomer.professional_postal_code : undefined;
      oCustomer.title = data.title ? data.title : oCustomer.title ? oCustomer.title : undefined;
      oCustomer.degree_institution = data.degree_institution ? data.degree_institution : oCustomer.degree_institution ? oCustomer.degree_institution : undefined;
      oCustomer.degree_course = data.degree_course ? data.degree_course : oCustomer.degree_course ? oCustomer.degree_course : undefined;
      oCustomer.degree_city = data.degree_city ? data.degree_city : oCustomer.degree_city ? oCustomer.degree_city : undefined;
      oCustomer.year_conclusion_degree = data.year_conclusion_degree ? data.year_conclusion_degree : oCustomer.year_conclusion_degree ? oCustomer.year_conclusion_degree : undefined;
      oCustomer.specialization_institution  = data.specialization_institution ? data.specialization_institution : oCustomer.specialization_institution ? oCustomer.specialization_institution : undefined;
      oCustomer.specialization_course  = data.specialization_course ? data.specialization_course : oCustomer.specialization_course ? oCustomer.specialization_course : undefined;
      oCustomer.year_conclusion_specialization  = data.year_conclusion_specialization ? data.year_conclusion_specialization : oCustomer.year_conclusion_specialization ? oCustomer.year_conclusion_specialization : undefined;
      oCustomer.master_institution  = data.master_institution ? data.master_institution : oCustomer.master_institution ? oCustomer.master_institution : undefined;
      oCustomer.master_course  = data.master_course ? data.master_course : oCustomer.master_course ? oCustomer.master_course : undefined;
      oCustomer.year_conclusion_master  = data.year_conclusion_master ? data.year_conclusion_master : oCustomer.year_conclusion_master ? oCustomer.year_conclusion_master : undefined;
      oCustomer.doc_institution  = data.doc_institution ? data.doc_institution : oCustomer.doc_institution ? oCustomer.doc_institution : undefined;
      oCustomer.doc_course  = data.doc_course ? data.doc_course : oCustomer.doc_course ? oCustomer.doc_course : undefined;
      oCustomer.year_conclusion_doc  = data.year_conclusion_doc ? data.year_conclusion_doc : oCustomer.year_conclusion_doc ? oCustomer.year_conclusion_doc : undefined;
      oCustomer.phd_institution  = data.phd_institution ? data.phd_institution : oCustomer.phd_institution ? oCustomer.phd_institution : undefined;
      oCustomer.phd_course  = data.phd_course ? data.phd_course : oCustomer.phd_course ? oCustomer.phd_course : undefined;
      oCustomer.year_conclusion_phd  = data.year_conclusion_phd ? data.year_conclusion_phd : oCustomer.year_conclusion_phd ? oCustomer.year_conclusion_phd : undefined;

      return await oCustomer.save();
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * This method return the document user with details
   * @param {Function(Error, object)} callback
   */
  Authenticated.getUserDetails = async function(options, callback) {
    const oCustomer = await options.accessToken.getClient(); 

    if(oCustomer){
      return oCustomer;
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * Get the active institution product
   * @param {string} host The application host endpoint string
   * @param {Function(Error, object)} callback
   */
  Authenticated.getInstitutionProduct = async function(options, host, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;
  
    if(oCustomer && host){
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});

      if(oClient){
        return await oClient.products.findOne({where:{default:true}});
      }
    
    }

    var err = new Error("Missing required fields");
    err.name = "You need the correct host to request this method.";
    err.statusCode = 403;
    return callback(err)
  };

  /**
   * This method must recieve the information about inscription and price to create transaction to recieve the value also put then on transaction application
   * @param {object} data All the data information we need to submit the user inscription
   * @param {Function(Error, object)} callback
   */
  Authenticated.submitInscription = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const token = typeof(data.token) == 'string' && data.token.length > 0 ? data.token : false;

    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const products = typeof(data.products) == 'object' && data.products instanceof Array && data.products.length > 0 ? data.products : false;

    const oPrice = typeof(data.oPrice) == 'object' ? data.oPrice : false;
    const associated = typeof(data.associated) == 'string' && data.associated.length > 0 ? data.associated : false;
    
    if(oCustomer && host && associated){
      const oCongress = await Authenticated.app.models.Congress.findById(congressId);
      const oInscriptionTemplate = {
        date: new Date(),
        type: `associated`,
        congressId: oCongress.id,
        oPrice
      }

      const oInscription = await oCustomer.inscriptions.create(oInscriptionTemplate);
      
      return oInscription;
    } else if(oCustomer && host && token){
      const oClient = await Authenticated.app.models.Client.findOne({where:{front: host}, include : ['congresses']})
      const aTransactions = await oCustomer.transactions.find();

      if(oClient){
        const oCongress = await Authenticated.app.models.Congress.findById(congressId);
        if(oCongress){
          let amount = 0; 
          for (let index = 0; index < oCongress.products.length; index++) {
            const cP = oCongress.products[index];
            for (let iChild = 0; iChild < products.length; iChild++) {
              const product = products[iChild];
              if(cP.text == product.name && cP.value == product.price){
                amount += cP.value;
              } 
            }
          }
          
          const oCapturedTransaction = await new pagarme(Authenticated.app.dataSources.pagarmeDs).capture({
            token,
            amount
          });

          const oTransactionTemplate = {
            amount: oCapturedTransaction.paid_amount,
            status: oCapturedTransaction.status,
            processing: oCapturedTransaction.status == 'waiting_payment' ? 'waiting' : 'done',
            date: new Date(),
            type: oCapturedTransaction.payment_method,
            congressId:congressId,
            ref: `Pagamento Congresso: ${oCongress.name}`,
            link: 'inscription',
            clientId: oClient.id,
            tx: oCapturedTransaction
          };
          
          const oTx = await oCustomer.transactions.create(oTransactionTemplate);

          const oInscriptionTemplate = {
            date: new Date(),
            type: `inscription`,
            congressId: oCongress.id,
            transactionId: oTx.id,
            oPrice
          }

          const oInscription = await oCustomer.inscriptions.create(oInscriptionTemplate);
          
          return {
            status: oTransactionTemplate.status,
            date: oTransactionTemplate.date,
            type: oCapturedTransaction.payment_method,
            boleto_url: oCapturedTransaction.boleto_url || '',
            boleto_barcode: oCapturedTransaction.boleto_barcode || '',
            tx: oCapturedTransaction
          };
        }
      }
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * This method must accept avaliator data and subscript then into event
   * @param {object} data The information about avaliator
   * @param {Function(Error, object)} callback
   */
  Authenticated.avaliatorSubscription = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    
    
    if(oCustomer && host && congressId){
      const oClient = await Authenticated.app.models.Client.findOne({where:{front: host}})
      if(oClient){
        const oCongress = await Authenticated.app.models.Congress.findById(congressId);
        if(oCongress){
          const oAvaliatorTemplate = {
            date: new Date(),
            congressId: oCongress.id,
            ...data
          }
          const oAvaliator = await oCustomer.avaliators.create(oAvaliatorTemplate);
          return oAvaliator;
        }
      }
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };


  /**
   * This method its used to return information public about our application
   * @param {string} host The host from front application you want know more about
   * @param {Function(Error, object)} callback
   */
  Authenticated.applicationDetailAuth = async function(options, host, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    host = typeof(host) == 'string' && host.length > 0 ? host : false;
    
    if(oCustomer && host){
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host},include:["congresses"]});
      //
      const aCongresses = await oClient.congresses.find({where:{status:"published"}, include:[{"works":[{"avaliations":["customer"]}]}, "books", "cordinators"]});

      if(oClient){
        return {
          name: oClient.name,
          logo: oClient.logo,
          logomark: oClient.logomark,
          contact_email: oClient.contact_email,
          contact_phone: oClient.contact_phone,
          congresses: aCongresses
        }
      }

      var err = new Error("Client not found");
      err.name = "Unfortunately the follow client was not found";
      err.statusCode = 404;
      return callback(err)
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };


  /**
   * This method allows the participant to submit the work with everything he needs to correctly upload the information about the work
   * @param {object} data The data contain the information about the work
   * @param {Function(Error, object)} callback
   */
  Authenticated.submitWorkParticipant = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const workId = typeof(data.workId) == 'string' && data.workId.length > 0 ? data.workId : false;
    const type = typeof(data.type) == 'string' && data.type.length > 0 ? data.type : false;
    
    if(oCustomer && host && congressId && workId){
      const oClient = await Authenticated.app.models.Client.findOne({where:{front: host}})
      const oCongress = await Authenticated.app.models.Congress.findById(congressId);
      const oWork = await oCongress.works.findById(workId);
      if(oClient && oCongress && oWork){
        const oWorkTemplate = {
          workId : data.workId,
          lectiveYear : data.lectiveYear,
          date : new Date(),
          workOf : data.workOf,
          discipline : data.discipline,
          responsableTeatcher : data.responsableTeatcher,
          coAuthors : data.coAuthors,
          linkCloudWork : data.linkCloudWork,
          resumeWork: data.resumeWork,
          studyObject: data.studyObject,
          researchRealized: data.researchRealized,
          productionDescription: data.productionDescription
        }
        oWork.submitionWork = oWorkTemplate;
        oWork.customerId = oCustomer.id;
        return await oWork.save();
      }
    }

    if(oCustomer && host && congressId && type){
      const oClient = await Authenticated.app.models.Client.findOne({where:{front: host}})
      const oCongress = await Authenticated.app.models.Congress.findById(congressId);
      if(oClient && oCongress){
        const oWorkTemplate = {
            workName : data.workName,
            congressId : oCongress.id,
            host : data.host,
            ies : data.ies,
            category : data.category,
            leaderName : oCustomer.name || "",
            leaderDoc : oCustomer.cpf,
            date : new Date(),
            customerId : oCustomer.id,
            type,
            submitionWork: {
              lectiveYear : data.lectiveYear || '',
              date : new Date(),
              workOf : data.workOf || '',
              discipline : data.discipline || '',
              responsableTeatcher : data.responsableTeatcher || '',
              coAuthors : data.coAuthors || '',
              linkCloudWork : data.linkCloudWork,
              resumeWork: data.resumeWork,
              studyObject: data.studyObject || '',
              researchRealized: data.researchRealized || '',
              productionDescription: data.productionDescription || ''
            }
        }
        const oWork = await Authenticated.app.models.Work.create(oWorkTemplate);
        oWork.submitionWork.workId = oWork.id;
        await oWork.save();
        return oWork;
      }
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * This method must give power to bvalidator make validation 
   * @param {object} data All the data information about work, validator and congress
   * @param {Function(Error, object)} callback
   */
  Authenticated.avaliateWorkByAvaliator = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const workId = typeof(data.workId) == 'string' && data.workId.length > 0 ? data.workId : false;

    const experimentalismOfProduct = typeof(data.experimentalismOfProduct) == 'string' ? data.experimentalismOfProduct : false;
    const productQuality = typeof(data.productQuality) == 'string' ? data.productQuality : false;
    const coerenceOfContent = typeof(data.coerenceOfContent) == 'string' ? data.coerenceOfContent : false;
    const descriptionAvaliationWorkAvaliator = typeof(data.descriptionAvaliationWorkAvaliator) == 'string' ? data.descriptionAvaliationWorkAvaliator : false;
    

    if(oCustomer && host && congressId && workId){
      
      const oClient = await Authenticated.app.models.Client.findOne({where:{front: host}})
      const oCongress = await Authenticated.app.models.Congress.findById(congressId);
      const oWork = await oCongress.works.findById(workId);

      if(typeof(oWork.result) == 'string' && oWork.result.length > 0){
        var err = new Error("Missing required work");
          err.name = "You cannot request that path work already have result.";
          err.statusCode = 405;
          return callback(err)
      }

      if(oClient && oCongress && oWork){

        const defaultWorkOf = typeof(data.defaultWorkOf) == 'string' && data.defaultWorkOf.length > 0 ? data.defaultWorkOf : false;

        const oAvaliationTemplate = {
          customerId: oCustomer.id,
          workId: workId,
          date: new Date(),
          experimentalismOfProduct,
          productQuality,
          coerenceOfContent,
          descriptionAvaliationWorkAvaliator,
          defaultWorkOf,
          nationalWorks: utils.processAllRadioBoxesToArray(data)
        }
        
        return await Authenticated.app.models.Avaliation.create(oAvaliationTemplate);
      
      }

    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * This method must recieve the information about hte cordinator and finish the avaliation
   * @param {boolean} data Data from user and avaliation
   * @param {Function(Error, object)} callback
   */
  Authenticated.conclusionWork = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const workId = typeof(data.workId) == 'string' && data.workId.length > 0 ? data.workId : false;
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const experimentalismOfProduct = typeof(data.experimentalismOfProduct) == 'string' ? +data.experimentalismOfProduct : typeof(data.experimentalismOfProduct) == 'number' ? data.experimentalismOfProduct : false;
    const productQuality = typeof(data.productQuality) == 'string'  ? +data.productQuality : typeof(data.productQuality) == 'number' ? data.productQuality : false;
    const coerenceOfContent = typeof(data.coerenceOfContent) == 'string' ? +data.coerenceOfContent : typeof(data.coerenceOfContent) == 'number' ? data.coerenceOfContent : false;
    const descriptionAvaliationWorkCordinator = typeof(data.descriptionAvaliationWorkCordinator) == 'string' ? data.descriptionAvaliationWorkCordinator : false;

    if(oCustomer && workId && host && congressId){
    
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});
      if(!oClient){
          var err = new Error("Missing required level");
          err.name = "You cannot request that path.";
          err.statusCode = 401;
          return callback(err)
      }

      const oCongress = await Authenticated.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
      if(!oCongress){
          var err = new Error("Missing required congress");
          err.name = "You cannot request that path Congress not found.";
          err.statusCode = 401;
          return callback(err)
      }

      const oWork = await Authenticated.app.models.Work.findOne({where:{and:[{_id:workId},{congressId:oCongress.id}]}});
      if(!oWork){
          var err = new Error("Missing required work");
          err.name = "You cannot request that path work not found.";
          err.statusCode = 401;
          return callback(err)
      }
     
      if(typeof(oWork.result) == 'string' && oWork.result.length > 0){
        var err = new Error("Missing required work");
          err.name = "You cannot request that path work already have result.";
          err.statusCode = 405;
          return callback(err)
      }

      const oCordinator = await Authenticated.app.models.Cordinator.findOne({where:{and:[{congressId:oCongress.id},{customerId:oCustomer.id}]}});
      if(!oCordinator){
        var err = new Error("Missing required cordinator");
        err.name = "You cannot request that path cordinator not found.";
        err.statusCode = 401;
        return callback(err)
      }    

      if(oCongress.type == 'nacional'){
        const defaultCordinatorWorkOfPresented = typeof(data.defaultCordinatorWorkOfPresented) == 'string' ? data.defaultCordinatorWorkOfPresented : false;
        oWork.defaultCordinatorWorkOfPresented = defaultCordinatorWorkOfPresented;
      }

      if(oCongress.type == 'nacional' && typeof(oWork.oldCongressId) == 'undefined'){

        var defaultCordinatorWorkOf = typeof(data.defaultCordinatorWorkOf) == 'string' && data.defaultCordinatorWorkOf.length > 0 ? data.defaultCordinatorWorkOf : false;
        if(defaultCordinatorWorkOf && descriptionAvaliationWorkCordinator){
          oWork.defaultCordinatorWorkOf = defaultCordinatorWorkOf;
          oWork.descriptionAvaliationWorkCordinator = descriptionAvaliationWorkCordinator;
          oWork.correctionDate = new Date();
        } else if(defaultCordinatorWorkOf) {
          oWork.defaultCordinatorWorkOf = defaultCordinatorWorkOf;
          oWork.resultDate = new Date();
        }

        if(typeof(oWork.correctionVideo) == 'string' && oWork.correctionVideo.length > 0){
          oWork.correctionHistories = typeof(oWork.correctionHistories) == 'object' && oWork.correctionHistories.length > 0 ? oWork.correctionHistories : [];
          oWork.correctionHistories.push(oWork.correctionVideo);
          oWork.correctionDate = new Date();
          oWork.correctionVideo = null;
        }

        return await oWork.save();

      } else if(oCongress.type == 'regional' || typeof(oWork.oldCongressId) == 'object' || typeof(oWork.oldCongressId) == 'string'  ){
        oWork.experimentalismOfProduct = experimentalismOfProduct ? experimentalismOfProduct : 0;
        oWork.productQuality = productQuality ? productQuality : 0;
        oWork.coerenceOfContent = coerenceOfContent ? coerenceOfContent : 0;
        oWork.descriptionAvaliationWorkCordinator = descriptionAvaliationWorkCordinator ? descriptionAvaliationWorkCordinator : 0;
        return await oWork.save();
      }
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
  };

  /**
   * This method must receive the request from cordinator to finish categories
   * @param {object} data The data from front
   * @param {Function(Error, object)} callback
   */
  Authenticated.FinishWork = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const categs = typeof(data.categs) == 'object' && data.categs.length > 0 ? data.categs : false;
    const region = typeof(data.region) == 'string' && data.region.length > 0 ? data.region : false;


    let aApprovedWorks = []
    let aReprovedWorks = []

    if(oCustomer && host){
    
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});
      if(!oClient){
          var err = new Error("Missing required level");
          err.name = "You cannot request that path.";
          err.statusCode = 401;
          return callback(err)
      }

      const oCongress = await Authenticated.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
      if(!oCongress){
          var err = new Error("Missing required congress");
          err.name = "You cannot request that path Congress not found.";
          err.statusCode = 401;
          return callback(err)
      }

      const oCordinator = await Authenticated.app.models.Cordinator.findOne({where:{and:[{congressId:oCongress.id},{customerId:oCustomer.id}]}});
      if(!oCordinator){
        var err = new Error("Missing required cordinator");
        err.name = "You cannot request that path cordinator not found.";
        err.statusCode = 401;
        return callback(err)
      }    

      const aWorks = await Authenticated.app.models.Work.find({where:{congressId:oCongress.id}})
      if(!aWorks){
        var err = new Error("Missing required work");
        err.name = "You cannot request that path work not found.";
        err.statusCode = 401;
        return callback(err)
      }    
    
      let oIes = {};
      if(typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0){
        for (let index = 0; index < oCongress.iesList.length; index++) {
          const ies = oCongress.iesList[index];
          oIes[ies.name] = ies;
        }
      }

      const approvedWorks = []

      for (let index = 0; index < aWorks.length; index++) {
        const oWork = aWorks[index];
        if( oWork && oIes[oWork.ies] && oCongress.type && typeof(oWork.result) != 'string' ){

          let comparer = false;

          if(oCongress.type == 'regional'){
            comparer = oIes[oWork.ies].region == region;
          } else if(oCongress.type == 'nacional'){
            comparer = true;
          }

          for (let index = 0; index < oCordinator.categs.length; index++) {
            const oCateg = oCordinator.categs[index];
            for (let i = 0; i < categs.length; i++) {
              const selectedCategory = categs[i];
              if(
                typeof(oWork.coerenceOfContent) == 'number' 
                && typeof(oWork.experimentalismOfProduct) == 'number' 
                && typeof(oWork.productQuality) == 'number' 
                && oWork.category.indexOf(oCateg) > -1 
                && oWork.category.indexOf(selectedCategory) > -1 
                && comparer
              ){
                const average = (oWork.coerenceOfContent + oWork.experimentalismOfProduct + oWork.productQuality) / 3;
                oWork.average = average;
                if( average >= oCongress.cutNote){
                  approvedWorks.push(oWork);
                } else {
                  aReprovedWorks.push({
                    id: oWork.id,
                    workName: oWork.workName,
                    average: oWork.average
                  })
                  oWork.result = 'reproved';
                  oWork.resultDate = new Date();
                  await oWork.save();
                }
              }
            } 
          }
        }
        
      }

      if(approvedWorks.length < 1){
        var err = new Error("Missing works");
        err.name = "Not find available works";
        err.statusCode = 411;
        return callback(err)
      }

      let approvedOrdered = utils.orderArrayByField(approvedWorks,'average');

      approvedOrdered.sort(function(a, b) {
        return a.average - b.average;
      });

      approvedOrdered = approvedOrdered.reverse()

      for (let index = 0; index < approvedOrdered.length; index++) {
        const oApprovedWork = approvedOrdered[index];
        if(index < oCongress.worksByCateg){
          oApprovedWork.result = 'approved';
          oApprovedWork.resultDate = new Date();
          console.log('Trabalho Aprovado: ', oApprovedWork.workName)
          await oApprovedWork.save();
          aApprovedWorks.push({
            id: oApprovedWork.id,
            workName: oApprovedWork.workName,
            average: oApprovedWork.average
          })
        } else {
          oApprovedWork.result = 'reproved';
          oApprovedWork.resultDate = new Date();
          console.log('Trabalho Reprovado: ', oApprovedWork.workName)
          await oApprovedWork.save();
          aReprovedWorks.push({
            id: oApprovedWork.id,
            workName: oApprovedWork.workName,
            average: oApprovedWork.average
          })
        }
      }

      let oActionTemplate = {
        date: new Date(),
        type: 'success',
        categs,
        region,
        data: {
            message: 'Finish of Work',
            aApprovedWorks,
            aReprovedWorks
        },
        cordinatorId: oCordinator.id,
        customerId: oCustomer.id,
        clientId: oClient.id
      }
  
      await Authenticated.app.models.ActionHistory.create(oActionTemplate);

      return approvedOrdered;
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
  };

  /**
   * This method must filter the avaliators and return a list base on category and congress
   * @param {object} data Data contain the host and category
   * @param {Function(Error, object)} callback
   */
  Authenticated.listAvaliatorsByCategory = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const category = typeof(data.category) == 'string' && data.category.length > 0 ? data.category : false;

    if(oCustomer && host && congressId && category){
    
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});
      if(!oClient){
          var err = new Error("Missing required level");
          err.name = "You cannot request that path.";
          err.statusCode = 401;
          return callback(err)
      }

      const oCongress = await Authenticated.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
      if(!oCongress){
          var err = new Error("Missing required congress");
          err.name = "You cannot request that path Congress not found.";
          err.statusCode = 401;
          return callback(err)
      }

      const oCordinator = await Authenticated.app.models.Cordinator.findOne({where:{and:[{congressId:oCongress.id},{customerId:oCustomer.id}]}});
      if(!oCordinator){
        var err = new Error("Missing required cordinator");
        err.name = "You cannot request that path cordinator not found.";
        err.statusCode = 401;
        return callback(err)
      }    

      let dynamicFilter = {where:{and:[]}, include: ['customer']}
      dynamicFilter.where.and[0] = {}
      dynamicFilter.where.and[0][category] = true;
      dynamicFilter.where.and[1] = { congressId };
      
      const aAvaliators = await Authenticated.app.models.Avaliator.find(dynamicFilter)
      return aAvaliators
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
  };

  /**
   * This method send message to the user according the congress and client.
   * @param {object} data Data from cordinator notification
   * @param {Function(Error, object)} callback
   */
  Authenticated.createCordinatorNotification = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const message = typeof(data.message) == 'string' && data.message.length > 0 ? data.message : false;
    const categs = typeof(data.categs) == 'object' && data.categs.length > 0 ? data.categs : false;
    const region = typeof(data.region) == 'string' && data.region.length > 0 ? data.region : false;

    if(oCustomer && host && congressId && message && categs){
    
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});
      if(!oClient){
          var err = new Error("Missing required level");
          err.name = "You cannot request that path.";
          err.statusCode = 401;
          return callback(err)
      }

      const oCongress = await Authenticated.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
      if(!oCongress){
          var err = new Error("Missing required congress");
          err.name = "You cannot request that path Congress not found.";
          err.statusCode = 401;
          return callback(err)
      }
      
      const oCordinator = await Authenticated.app.models.Cordinator.findOne({where:{and:[{congressId:oCongress.id},{customerId:oCustomer.id}]}});
      if(!oCordinator){
        var err = new Error("Missing required cordinator");
        err.name = "You cannot request that path cordinator not found.";
        err.statusCode = 401;
        return callback(err)
      }    
      
      const aWorks = await Authenticated.app.models.Work.find({where:{congressId:oCongress.id}})
      if(!aWorks){
        var err = new Error("Missing required work");
        err.name = "You cannot request that path work not found.";
        err.statusCode = 401;
        return callback(err)
      }   
      
      let oIes = {};
      if(typeof(oCongress.iesList) == 'object' && oCongress.iesList instanceof Array && oCongress.iesList.length > 0){
        for (let index = 0; index < oCongress.iesList.length; index++) {
          const ies = oCongress.iesList[index];
          oIes[ies.name] = ies;
        }
      }
    
      let contactCustomer = [];
      for (let index = 0; index < categs.length; index++) {
        const category = categs[index];
        for (let x = 0; x < aWorks.length; x++) {
          const work = aWorks[x];
          if(work && oIes[work.ies] && oCongress.type){
            let comparer = false;

            if(oCongress.type == 'regional'){
              comparer = oIes[work.ies].region == region;
            } else if(oCongress.type == 'nacional'){
              comparer = true;
            }

            if(work.category.indexOf(category) > -1 && typeof(work.avaliators) == 'object' && work.avaliators instanceof Array && work.avaliators.length > 0 && comparer){
              for (let i = 0; i < work.avaliators.length; i++) {
                const avaliator = work.avaliators[i];
                if(!contactCustomer[avaliator]){
                  contactCustomer.push(avaliator);
                }  
              } 
            }
            
          }
          
        }
      }
      
      return await Authenticated.contactCustomer(contactCustomer,message)
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
  };

  /**
   * This method send message to the user according the congress and client.
   * @param {object} data Data from cordinator notification
   * @param {Function(Error, object)} callback
   */
  Authenticated.createCordinatorNotificationWork = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const message = typeof(data.message) == 'string' && data.message.length > 0 ? data.message : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    const workId = typeof(data.workId) == 'string' && data.workId.length > 0 ? data.workId : false;

    if(oCustomer && host && congressId && message && workId){
    
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});
      if(!oClient){
          var err = new Error("Missing required level");
          err.name = "You cannot request that path.";
          err.statusCode = 401;
          return callback(err)
      }

      const oCongress = await Authenticated.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
      if(!oCongress){
          var err = new Error("Missing required congress");
          err.name = "You cannot request that path Congress not found.";
          err.statusCode = 401;
          return callback(err)
      }
      
      const oCordinator = await Authenticated.app.models.Cordinator.findOne({where:{and:[{congressId:oCongress.id},{customerId:oCustomer.id}]}});
      if(!oCordinator){
        var err = new Error("Missing required cordinator");
        err.name = "You cannot request that path cordinator not found.";
        err.statusCode = 401;
        return callback(err)
      }    
      
      const oWork = await Authenticated.app.models.Work.findById(workId)
      if(!oWork){
        var err = new Error("Missing required work");
        err.name = "You cannot request that path work not found.";
        err.statusCode = 401;
        return callback(err)
      }   
      
      
      let contactCustomer = [];

      if(typeof(oWork.avaliators) == 'object' && oWork.avaliators.length > 0){
        for (let index = 0; index < oWork.avaliators.length; index++) {
          const avaliator = oWork.avaliators[index];
          contactCustomer.push(avaliator); 
        }
      }
      
      return await Authenticated.contactCustomer(contactCustomer,message)
    }

    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
  };

  Authenticated.contactCustomer = async function(contactCustomer,message){
    const response = [];
    for (let index = 0; index < contactCustomer.length; index++) {
      const customer = await Authenticated.app.models.Customer.findById(contactCustomer[index]);
        if(customer){
          const oTokenTemplate = {
            email: customer.email,
            name: customer.name,
            url: `https://intercom.uhub.club`,
            event_name: "messageToAvaliator"
        }
        let objToMail = {
          ref:"d86eae60-8401-4df0-9589-787914de976d",
          type:"messageToAvaliator",
          subject: "Intercom - Comunicado",
          text: `<p>${message}<br><br><a href="https://intercom.uhub.club">Clique aqui para acessar trabalhos</a></p> `,
          header:"https://static-uhub.s3.amazonaws.com/topo-intercom-v2.jpg",
          footer:"https://static-uhub.s3.amazonaws.com/bottom-intercom.jpg",
          color:"#3085d6",
          ...oTokenTemplate
        };
        var send = await new sengrid().sendEmail(customer.email, 'Intercom - Comunicado', 'd86eae60-8401-4df0-9589-787914de976d', objToMail)    
        response.push(send)
      }
    }
    return response;
  }

  Authenticated.contactSingleCustomer = async function(customerId,message){
    
    const customer = await Authenticated.app.models.Customer.findById(customerId);
    if(customer){
        const oTokenTemplate = {
          email: customer.email,
          name: customer.name,
          url: `https://intercom.uhub.club`,
          event_name: "messageToAvaliator"
      }
      let objToMail = {
        ref:"d86eae60-8401-4df0-9589-787914de976d",
        type:"messageToAvaliator",
        subject: "Intercom - Comunicado",
        text: `<p>${message}<br><br><a href="https://intercom.uhub.club">Clique aqui para acessar trabalhos</a></p> `,
        header:"https://static-uhub.s3.amazonaws.com/topo-intercom-v2.jpg",
        footer:"https://static-uhub.s3.amazonaws.com/bottom-intercom.jpg",
        color:"#3085d6",
        ...oTokenTemplate
      };
      var send = await new sengrid().sendEmail(customer.email, 'Intercom - Comunicado', 'd86eae60-8401-4df0-9589-787914de976d', objToMail)    
      return send;
    }
    return false;
  }

  /**
   * This method must recieve the information about hte cordinator and finish the avaliation
   * @param {boolean} data Data from user and avaliation
   * @param {Function(Error, object)} callback
   */
  Authenticated.declassifiedWork = async function(options, data, callback) {
      const oCustomer = await options.accessToken.getClient(); 
      const workId = typeof(data.workId) == 'string' && data.workId.length > 0 ? data.workId : false;
      const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
      const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
      const message = typeof(data.message) == 'string' && data.message.length > 0 ? data.message : false;
      const experimentalismOfProduct = 0;
      const productQuality = 0;
      const coerenceOfContent = 0;
      const descriptionAvaliationWorkCordinator = 0;

      if(oCustomer && workId && host && congressId && message ){
      
        const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});
        if(!oClient){
            var err = new Error("Missing required level");
            err.name = "You cannot request that path.";
            err.statusCode = 401;
            return callback(err)
        }

        const oCongress = await Authenticated.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
        if(!oCongress){
            var err = new Error("Missing required congress");
            err.name = "You cannot request that path Congress not found.";
            err.statusCode = 401;
            return callback(err)
        }

        const oWork = await Authenticated.app.models.Work.findOne({where:{and:[{_id:workId},{congressId:oCongress.id}]}});
        if(!oWork){
            var err = new Error("Missing required work");
            err.name = "You cannot request that path work not found.";
            err.statusCode = 401;
            return callback(err)
        }
        
        const oCordinator = await Authenticated.app.models.Cordinator.findOne({where:{and:[{congressId:oCongress.id},{customerId:oCustomer.id}]}});
        if(!oCordinator){
          var err = new Error("Missing required cordinator");
          err.name = "You cannot request that path cordinator not found.";
          err.statusCode = 401;
          return callback(err)
        }    

        oWork.experimentalismOfProduct = experimentalismOfProduct ? experimentalismOfProduct : 0;
        oWork.productQuality = productQuality ? productQuality : 0;
        oWork.coerenceOfContent = coerenceOfContent ? coerenceOfContent : 0;
        oWork.descriptionAvaliationWorkCordinator = descriptionAvaliationWorkCordinator ? descriptionAvaliationWorkCordinator : 0;
        oWork.status = 'declassified';
        oWork.message = message;
        

        await Authenticated.contactSingleCustomer(oWork.customerId, message)

        return await oWork.save();

      }
      var err = new Error("Missing required fields");
      err.name = "You cannot request that path, we are missing the required fields.";
      err.statusCode = 403;
      return callback(err)
  }

  /**
   * This method must recieve the information about hte cordinator and finish the avaliation
   * @param {boolean} data Data from user and avaliation
   * @param {Function(Error, object)} callback
   */
  Authenticated.sendVideoToWork = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const workId = typeof(data.workId) == 'string' && data.workId.length > 0 ? data.workId : false;
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const videoUrl = typeof(data.videoUrl) == 'string' && data.videoUrl.length > 0 ? data.videoUrl : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    

    if( oCustomer && workId && host && congressId ){
    
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});
      if(!oClient){
          var err = new Error("Missing required level");
          err.name = "You cannot request that path.";
          err.statusCode = 401;
          return callback(err)
      }

      const oCongress = await Authenticated.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
      if(!oCongress){
          var err = new Error("Missing required congress");
          err.name = "You cannot request that path Congress not found.";
          err.statusCode = 401;
          return callback(err)
      }

      const oWork = await Authenticated.app.models.Work.findOne({where:{and:[{_id:workId},{congressId:oCongress.id}]}});
      if(!oWork){
          var err = new Error("Missing required work");
          err.name = "You cannot request that path work not found.";
          err.statusCode = 401;
          return callback(err)
      }

      if( ( typeof(oWork.result) == 'string' && oWork.result != 'approved' ) || typeof(oWork.result) != 'string' ){
        var err = new Error("Missing required work");
        err.name = "You cannot update work for that video.";
        err.statusCode = 403;
        return callback(err)
      }

      if( typeof(oWork.presentationVideo) == 'string' && oWork.presentationVideo.length > 0 ){
        var err = new Error("Missing required work");
        err.name = "Your video already have presentation video.";
        err.statusCode = 403;
        return callback(err)
      }
      
      oWork.presentationVideo = videoUrl
      return await oWork.save();

    }
    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
  }

  /**
   * This method must recieve the information about hte cordinator and finish the avaliation
   * @param {boolean} data Data from user and avaliation
   * @param {Function(Error, object)} callback
   */
   Authenticated.sendVideoToCorrection = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const workId = typeof(data.workId) == 'string' && data.workId.length > 0 ? data.workId : false;
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const videoUrl = typeof(data.videoUrl) == 'string' && data.videoUrl.length > 0 ? data.videoUrl : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    

    if( oCustomer && workId && host && congressId ){
    
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});
      if(!oClient){
          var err = new Error("Missing required level");
          err.name = "You cannot request that path.";
          err.statusCode = 401;
          return callback(err)
      }

      const oCongress = await Authenticated.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
      if(!oCongress){
          var err = new Error("Missing required congress");
          err.name = "You cannot request that path Congress not found.";
          err.statusCode = 401;
          return callback(err)
      }

      const oWork = await Authenticated.app.models.Work.findOne({where:{and:[{_id:workId},{congressId:oCongress.id}]}});
      if(!oWork){
          var err = new Error("Missing required work");
          err.name = "You cannot request that path work not found.";
          err.statusCode = 401;
          return callback(err)
      }

      if( typeof(oWork.defaultCordinatorWorkOf) == 'string' && oWork.defaultCordinatorWorkOf != 'alterationNeed' ){
        var err = new Error("Missing required work");
        err.name = "You cannot update work for that video.";
        err.statusCode = 403;
        return callback(err)
      }
      
      oWork.correctionVideo = videoUrl
      return await oWork.save();

    }
    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
  }

  /**
   * This method allows the participant to submit the work with everything he needs to correctly upload the information about the work
   * @param {object} data The data contain the information about the work
   * @param {Function(Error, object)} callback
  */
  Authenticated.submitionBook = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;

    const bookEditoriumName = typeof(data.bookEditoriumName) == 'string' && data.bookEditoriumName.length > 0 ? data.bookEditoriumName : false;
    const bookArea = typeof(data.bookArea) == 'string' && data.bookArea.length > 0 ? data.bookArea : false;
    const bookType = typeof(data.bookType) == 'string' && data.bookType.length > 0 ? data.bookType : false;
    const bookName = typeof(data.bookName) == 'string' && data.bookName.length > 0 ? data.bookName : false;
    const bookCoAuthors = typeof(data.bookCoAuthors) == 'string' && data.bookCoAuthors.length > 0 ? data.bookCoAuthors : false;
    const bookCloudLink = typeof(data.bookCloudLink) == 'string' && data.bookCloudLink.length > 0 ? data.bookCloudLink : false;
    const bookResume = typeof(data.bookResume) == 'string' && data.bookResume.length > 0 ? data.bookResume : false;
    
    if(oCustomer && host && congressId && bookEditoriumName && bookArea && bookType && bookName && bookCloudLink && bookResume){
      const oClient = await Authenticated.app.models.Client.findOne({where:{front: host}})
      const oCongress = await Authenticated.app.models.Congress.findById(congressId);
      if(oClient && oCongress){
        const oBookTemplate = {
          customerId: oCustomer.id,
          clientId: oClient.id,
          congressId: oCongress.id,
          date : new Date(),
          name: bookName,
          area: bookArea,
          type: bookType,
          editor: bookEditoriumName,
          coauthor: bookCoAuthors,
          link: bookCloudLink,
          resume: bookResume
        }
        return await Authenticated.app.models.Book.create(oBookTemplate);
      }
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * This method must brings the information about the customer certification
   * @param {string} congressId The congress identification to request data
   * @param {string} type Type of certificate do request data
   * @param {Function(Error, object)} callback
   */
  Authenticated.certificate = async function(options, congressId, type, callback) {
    let aWorks = {}
    const oCustomer = await options.accessToken.getClientData(); 
    congressId = typeof(congressId) == 'string' && congressId.length > 0 ? congressId : false;
    type = typeof(type) == 'string' && type.length > 0 ? type : false;

    if(oCustomer && congressId && type){
      const oCongress = await Authenticated.app.models.Congress.findById(congressId);

      if(oCongress){

        // if(type == 'avaliatorCert'){
        //   let filter = {
        //     where:{
        //       and:[
        //         {
        //           avaliators:{
        //             inq: [oCustomer.id]
        //           }
        //         },
        //         {congressId: oCongress.id}
        //       ]
        //     }
        //   }
        //   aWorks = await Authenticated.app.models.Work.find(filter);
        // }



        return {oCustomer,oCongress, aWorks, type};
      }

      var err = new Error("Congress not found");
      err.name = "Unfortunately the follow congress was not found";
      err.statusCode = 404;
      return callback(err)
    }

    var err = new Error("Missing required level");
    err.name = "You cannot request that path.";
    err.statusCode = 401;
    return callback(err)
  };

  /**
   * This metod recieve data from publicom to update data to accepted book
   * @param {boolean} data Data from user and avaliation
   * @param {Function(Error, object)} callback
   */
  Authenticated.sendPublicomLink = async function(options, data, callback) {
    const oCustomer = await options.accessToken.getClient(); 
    const bookId = typeof(data.bookId) == 'string' && data.bookId.length > 0 ? data.bookId : false;
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const bookLinkUrl = typeof(data.bookLinkUrl) == 'string' && data.bookLinkUrl.length > 0 ? data.bookLinkUrl : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;
    

    if( oCustomer && bookId && host && congressId && bookLinkUrl ){
    
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});
      if(!oClient){
          var err = new Error("Missing required level");
          err.name = "You cannot request that path.";
          err.statusCode = 401;
          return callback(err)
      }

      const oCongress = await Authenticated.app.models.Congress.findOne({where:{and:[{_id:congressId},{clientId: oClient.id}]}});
      if(!oCongress){
          var err = new Error("Missing required congress");
          err.name = "You cannot request that path Congress not found.";
          err.statusCode = 401;
          return callback(err)
      }

      const oBook = await Authenticated.app.models.Book.findOne({where:{and:[{_id:bookId},{congressId:oCongress.id}]}});
      if(!oBook){
          var err = new Error("Missing required work");
          err.name = "You cannot request that path work not found.";
          err.statusCode = 401;
          return callback(err)
      }

      if( typeof(oBook.status) != 'string' || oBook.status != 'ok' ){
        var err = new Error("Missing required book");
        err.name = "You cannot update book for that link.";
        err.statusCode = 403;
        return callback(err)
      }
      
      oBook.linkSubmition = bookLinkUrl
      return await oBook.save();

    }
    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
  }


  /**
   * TODO: Encript and decript file upload and exibithion
   * Upload File.
   */
  Authenticated.uploadFile = async function(options, data) {
  const oCustomer = await options.accessToken.getClient();

  // const qr = new QrcodeDecoder();
  // WORK WITH IMAGE
  if(oCustomer){
      const form = new multiparty.Form();
      let file = await new Promise((resolve, reject) => {
          form.parse(data, (err, fields, files) => {
              const file = files['file'][0]; 
              if (!file) 
                  reject('File was not found in form data.');
              else
                  resolve(file.path)
          }) 
      })
      const uploader = await new _aws(file).uploadFile();
      return uploader;
  }

  return false;
  } 

  

  /**
   * This metod recieve the user code to load all data from the user created by the Lawyer
   * @param {string} code The data contain the Code from the user
   * @param {Function(Error, object)} callback
   */
  Authenticated.loadUserDataByLegalCode = async function(options, data, callback){
    const oCustomer = await options.accessToken.getClient(); 
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const code = typeof(data.code) == 'string' && data.code.length > 0 ? data.code : false;
    

    if( oCustomer && code && host ){
    
      const oClient = await Authenticated.app.models.Client.findOne({where:{front:host}});
      if(!oClient){
          var err = new Error("Missing required level");
          err.name = "You cannot request that path.";
          err.statusCode = 401;
          return callback(err)
      }

      const oUser = await Authenticated.app.models.Customer.findOne({where:{code}});
      if(!oUser){
          var err = new Error("Wrong Code");
          err.name = "The code do you user is incorrect please check your code and try again or contact your lawyer to verify if everything is right";
          err.statusCode = 401;
          return callback(err)
      }


      oUser.email = oCustomer.email;
      oUser.uid = oCustomer.uid;
      oUser.username = oCustomer.username;
      oUser.password = oCustomer.password;
      
      await oCustomer.delete();
      return await oUser.save();
    }
    var err = new Error("Missing required fields");
    err.name = "You cannot request that path, we are missing the required fields.";
    err.statusCode = 403;
    return callback(err)
  }
}