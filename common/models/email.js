'use strict';

module.exports = function(Email) {

    /**
     * 
     * @param {string} sEmailRef 
     * @param {fn} fnCallback 
     * 
     *  @TODO Here we need to create some table to improve that
     *  ARVISADM
     *  @Detault_Variables
     *      header
     *      color
     *      text
     *      footer
     */
    Email.findByRef = async (sEmailRef,eventId) => {
        const oEmail = await Email.findOne({
            where: {
                ref: sEmailRef,
                eventId: eventId
            }
        })
        if(oEmail){
            return oEmail;
        }
        return {
            __data: {
                subject: "Olá",
                text: "",
                header: "https://event-staging-enastic.s3.amazonaws.com/16a837c49c2dd1fb5a26d3ff23b421e5c9815231756478ba4dc86e61d465ef6b.png",
                footer: "https://static-arvis.s3.amazonaws.com/images/email/enastic/footer.jpg",
                color: "#8024cc"
            }
        }
    }
};


