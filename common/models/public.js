'use strict';

module.exports = function(Public) {

    
/**
 * This method its used to return information public about our application
 * @param {string} host The host from front application you want know more about
 * @param {Function(Error, object)} callback
 */
Public.applicationDetail = async function(host, callback) {
    const oClient = await Public.app.models.Client.findOne({where:{front:host},include:["congresses"]});

    const aCongresses = await oClient.congresses.find({where:{status:"published"}});

    if(oClient){
        return {
            name: oClient.name,
            logo: oClient.logo,
            logomark: oClient.logomark,
            contact_email: oClient.contact_email,
            contact_phone: oClient.contact_phone,
            congresses: aCongresses
        }
    }

    var err = new Error("Client not found");
    err.name = "Unfortunately the follow client was not found";
    err.statusCode = 404;
    return callback(err)
};

/**
 * This method recieves all information to create pool of works
 * @param {object} data All the data information to create the work indication
 * @param {Function(Error, boolean)} callback
 */
Public.indicateWork = async function(data, callback) {
    const host = typeof(data.host) == 'string' && data.host.length > 0 ? data.host : false;
    const congressId = typeof(data.congressId) == 'string' && data.congressId.length > 0 ? data.congressId : false;

    if(host){
        const oInscriptedCategoryAndIes = await Public.app.models.Work.findOne({where:{and:[{ies: data.ies},{category: data.category}]}})

        if(!oInscriptedCategoryAndIes){
            const oClient = await Public.app.models.Client.findOne({where:{front:host},include:["congresses"]});

            if(oClient){
                const oCongress = await oClient.congresses.findById(congressId);
                data.date = new Date();
                const oWorkIndication = await oCongress.works.create(data)
                return oWorkIndication;
            }

        }

        var err = new Error("Client already has inscription");
        err.name = "Unfortunately the client already have indication for the follow category and ies";
        err.statusCode = 401;
        return callback(err);

    }

    var err = new Error("Client not found");
    err.name = "Unfortunately the follow client was not found";
    err.statusCode = 404;
    return callback(err);
};

};
