var defaultDataHelper = require('../helpers/loader');

module.exports = async (app, cb) => {
  
  if (process.env.APPLICATION_INITIALIZE_DATABASE === 'yes') {
    
    console.warn('Initializing Database');
    
    if(process.env.NODE_ENV = 'dev'){
  
      app.dataSources.mongoDS.setMaxListeners(999999999);
  
    }

    if(process.env.NODE_ENV = 'prod'){
  
      app.dataSources.mongoDS.setMaxListeners(999999999);
  
    }
    
    await new defaultDataHelper(app).initializeDatabase(false);

  }

  if (process.env.APPLICATION_UPDATE_DATABASE === 'yes') {
  
    console.warn('Updating Database');
  
  }

  return cb()

}
