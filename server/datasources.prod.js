module.exports = {
  "mongoDS": {
    "port": 27017,
    "url":  process.env.PROVIDER_DATABASE_MONGODB_CONNECTION_STRING,
    "name": "mongoDS",
    "authSource" : "admin",
    "connector": "mongodb"
  },
  "aws": {
    "name": "aws",
    "connector": "loopback-component-storage",
    "provider": "amazon",
    "key": process.env.PROVIDER_AWS_S3_ACCESS,
    "keyId": process.env.AWS_S3_SECRET
  },
  "pagarmeDs": {
    "connector": "rest",
    "debug": true,
    "operations": [
      {
        "functions": {
          "capture": ["token", "amount"]
        },
        "template": {
          "method": "POST",
          "url": `${process.env.PROVIDER_PAYMENT_PAGARME_URL}/transactions/{token}/capture`,
          "headers": {
            "accepts": "application/json",
            "content-type": "application/json"
          },
          "body": {
              "amount": "{amount}",
              "api_key": `${process.env.PROVIDER_PAYMENT_PAGARME_API_TOKEN}`
          }
        }
      },
      {
        "functions": {
          "getx": ["txid"]
        },
        "template": {
          "method": "GET",
          "url": `${process.env.PROVIDER_PAYMENT_PAGARME_URL}/transactions/{txid}`,
          "headers": {
            "accepts": "application/json",
            "content-type": "application/json"
          },
          "query": {
              "api_key": `${process.env.PROVIDER_PAYMENT_PAGARME_API_TOKEN}`
          }
        }
      } 
    ]
  },
  "wherebyDs": {
    "connector": "rest",
    "debug": true,
    "operations": [
      {
        "functions": {
          "create": ["isLocked", "roomNamePrefix", "mode", "startISODate", "endISODate"]
        },
        "template": {
          "method": "POST",
          "url": `${process.env.PROVIDER_CALL_WHEREBY_URL}/meetings`,
          "headers": {
            "content-type": "application/json",
            "Authorization": `Bearer ${process.env.PROVIDER_CALL_WHEREBY_API_TOKEN}`
          },
          "body": {
            "isLocked": "{isLocked}",
            "roomNamePrefix": "{roomNamePrefix}",
            "roomMode": "{mode}",
            "startDate": "{startISODate}",
            "endDate": "{endISODate}",
            "fields": [
              "hostRoomUrl"
            ] 
          }
        }
      }
    ]
  }
};  

