module.exports = class DefaultDataHelper {
  constructor (app) {
    this.app = app;
    this.rootUser;
    this.ethereum = require('../services/ethereum');
    this.utils = require('../../server/helpers/utils.js');
    this.sengrid = require('../../server/services/sendgrid.js');
    this.customerCredentials = require('./defaults/customer-credentials.json');
  }

  async initializeDatabase (clean) {
    console.debug('\n@@ Start Application Loader @@');
    if (clean)
    {
      console.debug(' - Cleaning Database');
      await this.cleanDatabase();
    }
    await this.createCustomerCredentials(this.app.models.Customer);
    console.debug('@@ Finish Application Loader @@\n\n');
    return;
  }
  
  async createCustomerCredentials (customerCredentialModel) {
    console.debug(' - Creating Customer Credentials')
    for (let customerCredential of this.customerCredentials) {
      let customerCredentialObj = {
        ...customerCredential
      }
      const oCustomer = await customerCredentialModel.create(customerCredentialObj)
    }
    
  }

  async cleanDatabase () {
    var models = this.app.models;
    for (var modelName in models) {
      if (models[modelName].destroyAll)
      try {
        await models[modelName].destroyAll();
      } catch (err) {
        console.warn(err);
      }
    }
  }
};

