const crypto = require('crypto');

module.exports = {
    randomString : (stringBase = 'hex', byteLength = 16) => 
    new Promise((resolve, reject) => {
        crypto.randomBytes(byteLength, (err, buffer) => {
        if (err) {
            reject(err);
        } else {
            resolve(buffer.toString(stringBase));
        }
        });
    }),
    orderArrayByField : (array,field) => {
        function compare(a, b) {
          if (a[field] < b[field]){
            return -1;
          }
          if (a[field] > b[field]){
            return 1;
          }
          return 0;
        }
        return array.sort(compare);
     },

    inputCheckboxToArray : (obj, keyTarget) =>
    {
        const aData = [];
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                const value = obj[key];
                if(key.indexOf(keyTarget) > -1 && value && value.indexOf('Selecione') < 0)
                    aData.push(value)
            }
        }
        return aData;
    },
    /**
     * This method runs into object searching for all objects keys with left brackets and create index to re-arrenge 
     * creating simple multi object array return 
     * @param {*} obj 
     */
     processAllRadioBoxesToArray : obj =>
     {
         const aIndexes = []
         for (const key in obj) {
             if (obj.hasOwnProperty(key)) {
                 const value = obj[key];
                 if(key.indexOf('[') > -1){
                     let identifier = key.split('[')[1].split(']')[0];
                     let fieldKey = key.split('[')[1].replace(']','');
                     if(typeof(aIndexes[identifier]) != 'object') aIndexes[identifier] = {};
                     aIndexes[fieldKey] = value;
                 }
             }
         }
         return aIndexes;
     },
    /**
     * This method runs into object searching for all objects keys with left brackets and create index to re-arrenge 
     * creating simple multi object array return 
     * @param {*} obj 
     */
    processAllCheckBoxesToArray : obj =>
    {
        const aIndexes = []
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                const value = obj[key];
                if(key.indexOf('[') > -1){
                    let identifier = key.split('[')[1].split(']')[0];
                    let fieldKey = key.split('[')[0];
                    if(typeof(aIndexes[identifier]) != 'object') aIndexes[identifier] = {};
                    aIndexes[identifier][fieldKey] = value;
                }
            }
        }
        return aIndexes;
    },
    /**
     * This method runs into object searching for all objects keys search by some key  with left brackets and create index to re-arrenge 
     * creating simple multi object array return 
     * @param {Object} obj Must be object contains front dynamic array that means fields keys with [someNumber] 
     * @param {string} sKey Must be the string you wanna search to filter the stuff man
     */
    processAllCheckBoxesByKey : (obj, sKey) =>
    {
        const aIndexes = []
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                const value = obj[key];
                if(key.indexOf('[') > -1 && key.indexOf(sKey) > -1){
                    let identifier = key.split('[')[1].split(']')[0];
                    let fieldKey = (key.split('[')[0]).replace(sKey,"");
                    if(typeof(aIndexes[identifier]) != 'object') aIndexes[identifier] = {};
                    aIndexes[identifier][fieldKey] = value;
                }
            }
        }
        return aIndexes;
    },
    generateRandomFloatInRange: (min, max) =>
    {
        return (Math.random() * (max - min + 1)) + min;
    }
}
