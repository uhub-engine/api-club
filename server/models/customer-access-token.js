'use strict';

module.exports = function(Customeraccesstoken) {

    /**
     * get client data
     */
    Customeraccesstoken.prototype.getClient = async function() {
        const customerData = await Customeraccesstoken.app.models.Customer.findOne({
            where: {
                _id: this.userId
            },
            include: [
                "customerProfiles",
                { "inscriptions": ["transaction"] },
                "avaliators",
                "works",
                "cordinators"
            ]
        });
        return customerData
    }


    Customeraccesstoken.prototype.getClientData = async function() {
        const customerData = await Customeraccesstoken.app.models.Customer.findOne({
            where: {
                _id: this.userId
            },
            include: [
                {"avaliations": ["work"]},
                "avaliators",
                "cordinators"
            ]
        });
        return customerData
    }

    
    
};
