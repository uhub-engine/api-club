'use strict';

module.exports = function(Customer) {

    Customer.prototype.getClientByTokenId = async function(id) {
        const accessToken = await Customer.app.models.CustomerAccessToken.findOne({where:{id}});
        return accessToken
    }

    Customer.prototype.createClientAccessTokenByTokenId = async function(data) {
        const accessToken = await Customer.app.models.CustomerAccessToken.create({
            id: data.accessToken,
            ttl: 1209600,
            created: new Date(),
            userId: this.id
        });
        return accessToken
    }

};
