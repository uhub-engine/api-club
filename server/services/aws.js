const fs = require('fs');
const async = require('async');
const AWS = require('aws-sdk');
const _utils = require('../helpers/utils')

module.exports = class aws {
    constructor (fileName) { 
        this.file = fileName;
        this.extension = this.file.split('.')[1];
    }

    async uploadFile () {
        const s3 = new AWS.S3({
            accessKeyId: process.env.PROVIDER_AWS_S3_ACCESS,
            secretAccessKey: process.env.PROVIDER_AWS_S3_SECRET
        });

        // Read content from the file
        // const fileContent = fs.readFileSync(this.file);


        this.filename = await _utils.randomString('hex', 32);
        this.filename += `.${this.extension}`;

        var file = this.file;
        var filename = this.filename;

        // Setting up S3 upload parameters
        const params = {
            Bucket: process.env.PROVIDER_AWS_S3_BUCKET_NAME,
            Key: filename, 
            ContentType: `image/${this.extension}`, //<-- this is what you need!
            ContentDisposition: `inline; filename=${filename}`, //<-- and this !
            ACL: "public-read"
        };


        // Uploading files to the bucket
        let uploadedFile = await new Promise(function(resolve, reject){
            s3.createMultipartUpload(params, function(mpErr, multipart){
                if(!mpErr){
                    fs.readFile(file, function(err, fileData){
                        var partSize = 1024 * 1024 * 5;
                        var parts = Math.ceil(fileData.length / partSize);

                        async.timesSeries(parts, function(partNum, next){
                
                            var rangeStart = partNum*partSize;
                            var end = Math.min(rangeStart + partSize, fileData.length);
                    
                            //console.log("uploading ", filename, " % ", (partNum/parts).toFixed(2));
                    
                            partNum++;  
                            async.retry((retryCb) => {
                                    s3.uploadPart({
                                    Body: fileData.slice(rangeStart, end),
                                    Bucket: process.env.PROVIDER_AWS_S3_BUCKET_NAME,
                                    Key: filename,
                                    PartNumber: partNum,
                                    UploadId: multipart.UploadId
                                }, (err, mData) => {
                                    //console.log(err, mData)
                                    retryCb(err, mData);
                                });
                            }, (err, data)  => {
                                //console.log(data, {ETag: data.ETag, PartNumber: partNum});
                                next(err, {ETag: data.ETag, PartNumber: partNum});
                            });
                        }, 
                        (err, dataPacks) => {
                            //console.log(dataPacks)
                            s3.completeMultipartUpload({
                                Bucket: process.env.PROVIDER_AWS_S3_BUCKET_NAME,
                                Key: filename,
                                MultipartUpload: {
                                    Parts: dataPacks
                                },
                                UploadId: multipart.UploadId
                            }, function(err, res){
                                if(!err)
                                    resolve(res);
                                else
                                    reject(err);
                            });
                        });
                    });
                }else{
                    reject(mpErr);
                }
            });
        }) 
        
        return uploadedFile;
    }

};
