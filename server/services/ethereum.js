const Web3 = require('web3');
const keythereum = require('keythereum')
const CryptoJS = require('crypto-js')
const EthereumTx = require('ethereumjs-tx')
const { generateMnemonic, EthHdWallet } = require('eth-hd-wallet')
const nodeProvider = process.env.PROVIDER_ETHEREUM_NODE_URL;

const web3 = new Web3(new Web3.providers.HttpProvider(nodeProvider))


const ethereum =  {}

  ethereum.generateMnemonic = () => {
    let randomMnemonic = generateMnemonic();
    return randomMnemonic;
  }

  ethereum.generateWithMnemonic = sMnemonic => {
    const wallet = EthHdWallet.fromMnemonic(sMnemonic)
    wallet.generateAddresses(1)
    return {
      "publicKey" : wallet.getAddresses()[0],
      "privateKey" : wallet.getPrivateKey(wallet.getAddresses()[0]).toString('hex') 
    }
  }

  ethereum.generate = password => {

    // Initiate empty wallet object
    let wallet = {}
  
    // Generate private key
    let dk = keythereum.create()
  
    // Encrypt private key and collect ciphertext
    let privateKey = dk.privateKey.toString('hex')
    wallet.encryptedKey = CryptoJS.AES.encrypt(privateKey, password).toString()
  
    // Generate address and add '0x' prefix
    let keyObject = keythereum.dump(password, dk.privateKey, dk.salt, dk.iv)
    wallet.address = '0x' + keyObject.address
  
    return wallet
  
  }
  
  ethereum.decryptPrivateKey = (encryptedKey, password) => {
  
    // Decrypt private key and convert it to hex string
    return CryptoJS.AES.decrypt(encryptedKey, password).toString(CryptoJS.enc.Utf8)
  
  }

  ethereum.getBalance = async account => {
    let balance = await web3.eth.getBalance(account.address);
    let balanceEth = web3.utils.fromWei(balance.toString(10));
    return balanceEth;
  }

  ethereum.sendEthereum = async (account, address, amount) => {
    
    // Get nonce
    let nonce = 0
    return await web3.eth.getTransactionCount(account.address)
    .then((count) => {
      nonce = count
      //console.log("Nonce: " + nonce)
    })
    // Get recommended gas price
    .then(() => web3.eth.getGasPrice())
    // Construct transaction
    .then((gasPrice) => {
      // Log gas price
      //console.log("Gas price: " + gasPrice)
      // Convert amount to wei
      amount = web3.utils.toWei(amount, 'ether')
  
      const txParams = {
        nonce: web3.utils.toHex(nonce),
        to: address,
        gasPrice: web3.utils.toHex(gasPrice),
        gasLimit: web3.utils.toHex(290000),
        value: web3.utils.toHex(amount),
        chainId: 4
      }
  
      // Log params to console
      //console.log(txParams)
  
      // Create and sign transaction
      const tx = new EthereumTx(txParams)
      tx.sign(Buffer.from(account.privateKey, 'hex'))
  
      // Return serialized transaction data
      return "0x" + tx.serialize().toString('hex')
    })
    // Send transaction and resolve promise chain with Tx Hash
    .then(function(txData) {
      return new Promise((resolve, reject) => {
        web3.eth.sendSignedTransaction(txData)
        .on('transactionHash', (txHash) => resolve(txHash))
        .on('error', (err) => reject(err))
      })
    })
  }

  ethereum.createLecture = async function(fromAddress, lecture, privateKey) {
    const contractAddress = process.env.PROVIDER_ETHEREUM_ETH_ADDRESS_CONTRACT_LECTURE;

    //console.log(`web3 version: ${web3.version}`)
    // Who holds the token now?
    var myAddress = fromAddress;
    
    // Determine the nonce
    var count = await web3.eth.getTransactionCount(myAddress);
    //console.log(`num transactions so far: ${count}`);
    // MineFILToekn contract ABI Array
    var abiArray =  [
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "internalType": "uint256",
            "name": "_speakerId",
            "type": "uint256"
          }
        ],
        "name": "scannedEvent",
        "type": "event"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "address",
            "name": "",
            "type": "address"
          }
        ],
        "name": "customers",
        "outputs": [
          {
            "internalType": "bool",
            "name": "",
            "type": "bool"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "name": "speakers",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "id",
            "type": "uint256"
          },
          {
            "internalType": "address",
            "name": "owner",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "speakersCount",
        "outputs": [
          {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "address",
            "name": "owner",
            "type": "address"
          },
          {
            "internalType": "address[]",
            "name": "customer",
            "type": "address[]"
          }
        ],
        "name": "addSpeaker",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [
          {
            "internalType": "uint256",
            "name": "speaker",
            "type": "uint256"
          }
        ],
        "name": "getChildOfTheMapping",
        "outputs": [
          {
            "internalType": "address[]",
            "name": "",
            "type": "address[]"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": false,
        "inputs": [
          {
            "internalType": "uint256",
            "name": "_speakerId",
            "type": "uint256"
          }
        ],
        "name": "scan",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
      }
    ]
    
    // The address of the contract to record data 
    var contract = new web3.eth.Contract(abiArray, contractAddress, {
        from: myAddress
    });
    
    // I chose gas price and gas limit based on what ethereum wallet was recommending for a similar transaction. You may need to change the gas price!
    // Use Gwei for the unit of gas price
    var gasPriceGwei = 1;
    var gasLimit = 600000;

    

    // Chain ID of Ropsten Test Net is 3, replace it to 1 for Main Net
    var chainId = 4;
    var rawTransaction = {
        "from": myAddress,
        "nonce": "0x" + count.toString(16),
        "gasPrice": web3.utils.toHex(gasPriceGwei * 1e9),
        "gasLimit": web3.utils.toHex(gasLimit),
        "to": contractAddress,
        "value": "0x0",
        "data": contract.methods.addSpeaker(lecture, ['0x0000000000000000000000000000000000000000']).encodeABI(),
        "chainId": chainId
    };

    console.log(rawTransaction)

    //console.log(`Raw of Transaction: \n${JSON.stringify(rawTransaction, null, '\t')}\n------------------------`);
    // The private key for myAddress in .env
    //throw 'NEED URGENTLY REMOVE PRIVATES KEY - IMPLEMENT ENCRIPTED';
    var privKey = new Buffer.from(privateKey, 'hex');
    var tx = new EthereumTx(rawTransaction);
    tx.sign(privKey);
    console.log('Transação Assinada',tx)

    var serializedTx = tx.serialize();
    // Comment out these four lines if you don't really want to send the TX right now
    console.log(`Attempting to send signed tx:  ${serializedTx.toString('hex')}\n------------------------`);
    
    let hash = await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
    
    return hash;
  };

  ethereum.scanLecture = async function(fromAddress, lecture, privateKey) {
    try {
      const contractAddress = process.env.PROVIDER_ETHEREUM_ETH_ADDRESS_CONTRACT_LECTURE;

      //console.log(`web3 version: ${web3.version}`)
      // Who holds the token now?
      var myAddress = fromAddress;
      
      // Determine the nonce
      var count = await web3.eth.getTransactionCount(myAddress);
      //console.log(`num transactions so far: ${count}`);
      // MineFILToekn contract ABI Array
      var abiArray =  [
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "_speakerId",
              "type": "uint256"
            }
          ],
          "name": "scannedEvent",
          "type": "event"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "",
              "type": "address"
            }
          ],
          "name": "customers",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "name": "speakers",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "id",
              "type": "uint256"
            },
            {
              "internalType": "address",
              "name": "owner",
              "type": "address"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "speakersCount",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "owner",
              "type": "address"
            },
            {
              "internalType": "address[]",
              "name": "customer",
              "type": "address[]"
            }
          ],
          "name": "addSpeaker",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "speaker",
              "type": "uint256"
            }
          ],
          "name": "getChildOfTheMapping",
          "outputs": [
            {
              "internalType": "address[]",
              "name": "",
              "type": "address[]"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_speakerId",
              "type": "uint256"
            }
          ],
          "name": "scan",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        }
      ]
      
      // The address of the contract to record data 
      var contract = new web3.eth.Contract(abiArray, contractAddress, {
          from: myAddress
      });
      
      // I chose gas price and gas limit based on what ethereum wallet was recommending for a similar transaction. You may need to change the gas price!
      // Use Gwei for the unit of gas price
      var gasPriceGwei = 1;
      var gasLimit = 600000;    

      // Chain ID of Ropsten Test Net is 3, replace it to 1 for Main Net
      var chainId = 4;
      var rawTransaction = {
          "from": myAddress,
          "nonce": "0x" + count.toString(16),
          "gasPrice": web3.utils.toHex(gasPriceGwei * 1e9),
          "gasLimit": web3.utils.toHex(gasLimit),
          "to": contractAddress,
          "value": "0x0",
          "data": contract.methods.scan(lecture).encodeABI(),
          "chainId": chainId
      };

      console.log(rawTransaction)

      //console.log(`Raw of Transaction: \n${JSON.stringify(rawTransaction, null, '\t')}\n------------------------`);
      // The private key for myAddress in .env
      //throw 'NEED URGENTLY REMOVE PRIVATES KEY - IMPLEMENT ENCRIPTED';
      var privKey = new Buffer.from(privateKey, 'hex');
      var tx = new EthereumTx(rawTransaction);
      tx.sign(privKey);
      console.log('Transação Assinada',tx)

      var serializedTx = tx.serialize();
      // Comment out these four lines if you don't really want to send the TX right now
      console.log(`Attempting to send signed tx:  ${serializedTx.toString('hex')}\n------------------------`);
      
      let hash = await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
      return hash;
    } catch (error) {
     return false;
    }
  };

  ethereum.createStand = async function(fromAddress, stand, privateKey, customerAddress) {
    try{
      const contractAddress = process.env.PROVIDER_ETHEREUM_ETH_ADDRESS_CONTRACT_STAND;

      //console.log(`web3 version: ${web3.version}`)
      // Who holds the token now?
      var myAddress = fromAddress;
      
      // Determine the nonce
      var count = await web3.eth.getTransactionCount(myAddress);
      //console.log(`num transactions so far: ${count}`);
      // MineFILToekn contract ABI Array
      var abiArray =  [
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "_standerId",
              "type": "uint256"
            }
          ],
          "name": "scannedStand",
          "type": "event"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "",
              "type": "address"
            }
          ],
          "name": "customers",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "name": "standers",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "id",
              "type": "uint256"
            },
            {
              "internalType": "address",
              "name": "owner",
              "type": "address"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "standersCount",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "owner",
              "type": "address"
            },
            {
              "internalType": "address[]",
              "name": "customer",
              "type": "address[]"
            }
          ],
          "name": "addStander",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "stander",
              "type": "uint256"
            }
          ],
          "name": "getChildOfTheMapping",
          "outputs": [
            {
              "internalType": "address[]",
              "name": "",
              "type": "address[]"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_standerId",
              "type": "uint256"
            }
          ],
          "name": "scan",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        }
      ]
      
      // The address of the contract to record data 
      var contract = new web3.eth.Contract(abiArray, contractAddress, {
          from: myAddress
      });
      
      // I chose gas price and gas limit based on what ethereum wallet was recommending for a similar transaction. You may need to change the gas price!
      // Use Gwei for the unit of gas price
      var gasPriceGwei = 1;
      var gasLimit = 600000;
  
      
  
      // Chain ID of Ropsten Test Net is 3, replace it to 1 for Main Net
      var chainId = 4;
      var rawTransaction = {
          "from": myAddress,
          "nonce": "0x" + count.toString(16),
          "gasPrice": web3.utils.toHex(gasPriceGwei * 1e9),
          "gasLimit": web3.utils.toHex(gasLimit),
          "to": contractAddress,
          "value": "0x0",
          "data": contract.methods.addStander(stand, [customerAddress]).encodeABI(),
          "chainId": chainId
      };
  
      //console.log(rawTransaction)
  
      //console.log(`Raw of Transaction: \n${JSON.stringify(rawTransaction, null, '\t')}\n------------------------`);
      // The private key for myAddress in .env
      //throw 'NEED URGENTLY REMOVE PRIVATES KEY - IMPLEMENT ENCRIPTED';
      var privKey = new Buffer.from(privateKey, 'hex');
      var tx = new EthereumTx(rawTransaction);
      tx.sign(privKey);
      //console.log('Transação Assinada',tx)
  
      var serializedTx = tx.serialize();
      // Comment out these four lines if you don't really want to send the TX right now
      //console.log(`Attempting to send signed tx:  ${serializedTx.toString('hex')}\n------------------------`);
      let hash = await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
      console.log(hash);
      return hash;
    } catch (error) {
     return false;
    }
   
  };

  ethereum.scanStand = async function(fromAddress, stand, privateKey) {
    try {
      const contractAddress = process.env.PROVIDER_ETHEREUM_ETH_ADDRESS_CONTRACT_STAND;

      //console.log(`web3 version: ${web3.version}`)
      // Who holds the token now?
      var myAddress = fromAddress;
      
      // Determine the nonce
      var count = await web3.eth.getTransactionCount(myAddress);
      //console.log(`num transactions so far: ${count}`);
      // MineFILToekn contract ABI Array
      var abiArray =  [
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "uint256",
              "name": "_standerId",
              "type": "uint256"
            }
          ],
          "name": "scannedStand",
          "type": "event"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "address",
              "name": "",
              "type": "address"
            }
          ],
          "name": "customers",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "name": "standers",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "id",
              "type": "uint256"
            },
            {
              "internalType": "address",
              "name": "owner",
              "type": "address"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "standersCount",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "owner",
              "type": "address"
            },
            {
              "internalType": "address[]",
              "name": "customer",
              "type": "address[]"
            }
          ],
          "name": "addStander",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "stander",
              "type": "uint256"
            }
          ],
          "name": "getChildOfTheMapping",
          "outputs": [
            {
              "internalType": "address[]",
              "name": "",
              "type": "address[]"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "uint256",
              "name": "_standerId",
              "type": "uint256"
            }
          ],
          "name": "scan",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        }
      ]
      
      // The address of the contract to record data 
      var contract = new web3.eth.Contract(abiArray, contractAddress, {
          from: myAddress
      });
      
      // I chose gas price and gas limit based on what ethereum wallet was recommending for a similar transaction. You may need to change the gas price!
      // Use Gwei for the unit of gas price
      var gasPriceGwei = 1;
      var gasLimit = 600000;    

      // Chain ID of Ropsten Test Net is 3, replace it to 1 for Main Net
      var chainId = 4;
      var rawTransaction = {
          "from": myAddress,
          "nonce": "0x" + count.toString(16),
          "gasPrice": web3.utils.toHex(gasPriceGwei * 1e9),
          "gasLimit": web3.utils.toHex(gasLimit),
          "to": contractAddress,
          "value": "0x0",
          "data": contract.methods.scan(stand).encodeABI(),
          "chainId": chainId
      };

      //console.log(rawTransaction)

      //console.log(`Raw of Transaction: \n${JSON.stringify(rawTransaction, null, '\t')}\n------------------------`);
      // The private key for myAddress in .env
      //throw 'NEED URGENTLY REMOVE PRIVATES KEY - IMPLEMENT ENCRIPTED';
      var privKey = new Buffer.from(privateKey, 'hex');
      var tx = new EthereumTx(rawTransaction);
      tx.sign(privKey);
      //console.log('Transação Assinada',tx)

      var serializedTx = tx.serialize();
      // Comment out these four lines if you don't really want to send the TX right now
      //console.log(`Attempting to send signed tx:  ${serializedTx.toString('hex')}\n------------------------`);
      
      let hash = await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
      
      return hash;

      
    } catch (error) {
     return false;
    }
  };

  ethereum.visitStand = async function(stand, customer) {
    try {
      const fromAddress = process.env.PROVIDER_ETHEREUM_ETH_ADDRESS_MASTER_ACCOUNT;
      const privateKey = process.env.PROVIDER_ETHEREUM_ETH_ADDRESS_MASTER_ACCOUNT_KEY;
      const contractAddress = process.env.PROVIDER_ETHEREUM_ETH_ADDRESS_CONTRACT_STAND;
      const eventNode = `NAME_TO_RECORD_ON_ETHEREUM_BLOCKCHAIN`;

      //console.log(`web3 version: ${web3.version}`)
      // Who holds the token now?
      var myAddress = fromAddress;
      
      // Determine the nonce
      var count = await web3.eth.getTransactionCount(myAddress);
      //console.log(`num transactions so far: ${count}`);
      // MineFILToekn contract ABI Array
      var abiArray =   [
        {
          "anonymous": false,
          "inputs": [
            {
              "indexed": true,
              "internalType": "address",
              "name": "previousOwner",
              "type": "address"
            },
            {
              "indexed": true,
              "internalType": "address",
              "name": "newOwner",
              "type": "address"
            }
          ],
          "name": "OwnershipTransferred",
          "type": "event"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "isOwner",
          "outputs": [
            {
              "internalType": "bool",
              "name": "",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [],
          "name": "owner",
          "outputs": [
            {
              "internalType": "address",
              "name": "",
              "type": "address"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [],
          "name": "renounceOwnership",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "address",
              "name": "newOwner",
              "type": "address"
            }
          ],
          "name": "transferOwnership",
          "outputs": [],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "string",
              "name": "pid",
              "type": "string"
            },
            {
              "internalType": "address",
              "name": "stand",
              "type": "address"
            },
            {
              "internalType": "address",
              "name": "customer",
              "type": "address"
            }
          ],
          "name": "pendingRequest",
          "outputs": [
            {
              "internalType": "bool",
              "name": "scanned",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": false,
          "inputs": [
            {
              "internalType": "string",
              "name": "pid",
              "type": "string"
            },
            {
              "internalType": "address",
              "name": "stand",
              "type": "address"
            },
            {
              "internalType": "address",
              "name": "customer",
              "type": "address"
            }
          ],
          "name": "visit",
          "outputs": [
            {
              "internalType": "bool",
              "name": "success",
              "type": "bool"
            }
          ],
          "payable": false,
          "stateMutability": "nonpayable",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "string",
              "name": "pid",
              "type": "string"
            },
            {
              "internalType": "uint256",
              "name": "idx",
              "type": "uint256"
            }
          ],
          "name": "getDetails",
          "outputs": [
            {
              "internalType": "address",
              "name": "stand",
              "type": "address"
            },
            {
              "internalType": "address",
              "name": "customer",
              "type": "address"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        },
        {
          "constant": true,
          "inputs": [
            {
              "internalType": "string",
              "name": "pid",
              "type": "string"
            }
          ],
          "name": "getDetailsCount",
          "outputs": [
            {
              "internalType": "uint256",
              "name": "",
              "type": "uint256"
            }
          ],
          "payable": false,
          "stateMutability": "view",
          "type": "function"
        }
      ]
      
      // The address of the contract to record data 
      var contract = new web3.eth.Contract(abiArray, contractAddress, {
          from: myAddress
      });
      
      // I chose gas price and gas limit based on what ethereum wallet was recommending for a similar transaction. You may need to change the gas price!
      // Use Gwei for the unit of gas price
      var gasPriceGwei = 1;
      var gasLimit = 600000;    

      // Chain ID of Ropsten Test Net is 3, replace it to 1 for Main Net
      var chainId = 4;
      var rawTransaction = {
          "from": myAddress,
          "nonce": "0x" + count.toString(16),
          "gasPrice": web3.utils.toHex(gasPriceGwei * 1e9),
          "gasLimit": web3.utils.toHex(gasLimit),
          "to": contractAddress,
          "value": "0x0",
          "data": contract.methods.visit(eventNode, stand, customer).encodeABI(),
          "chainId": chainId
      };

      //console.log(rawTransaction)

      //console.log(`Raw of Transaction: \n${JSON.stringify(rawTransaction, null, '\t')}\n------------------------`);
      // The private key for myAddress in .env
      //throw 'NEED URGENTLY REMOVE PRIVATES KEY - IMPLEMENT ENCRIPTED';
      var privKey = new Buffer.from(privateKey, 'hex');
      var tx = new EthereumTx(rawTransaction);
      tx.sign(privKey);
      //console.log('Transação Assinada',tx)

      var serializedTx = tx.serialize();
      // Comment out these four lines if you don't really want to send the TX right now
      //console.log(`Attempting to send signed tx:  ${serializedTx.toString('hex')}\n------------------------`);
      
      let hash = await web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'))
      
      return hash;

      
    } catch (error) {
     return false;
    }
  };

module.exports = ethereum;