'use strict';


const admin = require("firebase-admin");

const serviceAccount = {
    "type": "service_account",
    "project_id": "uhubclub",
    "private_key_id": "4691ca7ba3dd99d3c3cf98a82726e727b9631c55",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDtju1kE7pPlsOF\nKhHu+htumZ5rS9bFKvzRWckU6Vtp4rVCryjFCRh1vnnpBaQOnfKGRLmA48FJ6Zro\nYt4iSKi6mPo+UhmLVMJReHvUWZtEcWGNa7wNQS6b0sUt+E4VL5KASf0iNGnsEfk0\n+Zx8tnoC5d7GYr8cDzGW6HXVRzcFA6pP+CAJTDaKtJw2WAZVwCoFu3fkFTpejMOM\nRKRWzQSoNPC9VJzK87wza0SvSVbK8ehmZ6DF9LOUWmWPW9W8uRvfTJBqKqY0pI8e\nphT6Wx4FCKTPEJe4m1JlkxEqrQxNsU0A9yGKznDZXJ62X2ztzxF4kiBg1cAajotv\nletaCsYfAgMBAAECggEAHqkMTZbYPI52VLkN0Hy6TqeIEUwb/52hPLnsYIUP1dY7\nU8x8pBWc8RFw9ZjTSEmtdBPpp0998naylhywk9cx48d7wNBi2J9LZy14OfuItSg3\nEYQepM1NYTIuXdUG6ugVbfZes+v04dGeCu7OejApBje57XvwM4Rof5QoJStIWB6V\nyJPd91AAKZfPBznwmo6vex9HFjio+fpX2w+T4DygqoP6+xohUSlbZNcQeE6jRd6s\ngJlh1o12B/PWsIndOyqpCwKJieCFMALuFbTNdpJmLFT/E1J9cvOM3doZG3T7TCBm\nIjPgt8l6ralimwF4Gunvt6NONna0/+ilOOb0Wgr6sQKBgQD//BGwaEr5fRZJm4aM\n5u3ACTz1IpALkmJ4bBzybk3Wnk5zQUogfkkCv1g6tNrTs0i8fvg4MlAzM+gRd2I9\nRV4Xm6zBZ4l7wFy1EZCR4lZ1Dt2nWsp938geV8QgxRJTMH3Sr2qNrnprr1tXsZfq\ndEZndaq+IHu9cUwabtwkQ3lwrwKBgQDtkpND73QsEkuah4pHtPfpZPZqf4yfQnaJ\nfYp/kTnzXWXwHL83yO5xDXXzvK/eKSvFR+jhcZHe3jQNmBY7zsfMoZSvQ7LLIeEo\nhnOlxENJb9wqpVzC9O0tM6gE2soUrcXk3We/OduE/Fzh+xt2auJWKKTlK+Elhf71\n8TWV96v9kQKBgF3rMN5FVYS15TB6dGJL89zqRsg1tx3XxnMiT3iPCdX9XREiWj0T\nuFveslNPuZ3EgKJP/2g25X/zwmhuIcgTJFOLFVYcU6/owtrYJ++uWEQ4gRB+2h5W\ntYiUcOwJAhEtkGx1WaXD5zyNKPlCulcV9CLPZCa/qka2Q9LK69AORYcXAoGAaS4G\nowIlpclnzX+Kmhql06AOdLIJuQy83DxHkBjk1jkYg4jGo9UCs+yotwNnvWzu4wXm\nNcGJOmmYFBisZ0gyLweugzXut35FaCK2msJco+uxRvQ1MF8tjmCbV6Lh6uvQfHYF\nk6Ty6lqsaKQ60V+a21hdQNT/dhRv/8UN21puu/ECgYAkE1fbGS+6Z1HFLTOJPgLb\nJFV5fsTn2xhYhy8XRXUn3DzZC7HgFYzWFZiG8RIe3idfUhrx8YUoxXLd4307bzDm\nuv9fBCdIFn8urIKY86GSx49ynJKovXan+aNmDvP2J0fsIfmoUigkwAONoIb+t68S\nHKiN8BUOh1bDl45QzNkyiQ==\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-rf5h2@uhubclub.iam.gserviceaccount.com",
    "client_id": "106888108848681941388",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-rf5h2%40uhubclub.iam.gserviceaccount.com"
 }
  

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://uhubclub-default-rtdb.firebaseio.com"
});

const frb = {};


frb.verifyIdToken = function(idToken){
    return admin.auth().verifyIdToken(idToken);
}

module.exports = frb;