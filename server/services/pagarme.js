
module.exports = class pagarmeClass {

    constructor (ds) { 
        this.oDataSource = ds;
    }


    async capture(data)
    {

        const token = typeof(data.token) == 'string' && data.token.length > 3 ? data.token : false;
        const amount = typeof(data.amount) == 'number' ? data.amount : false;

        try{
            const result = await this.oDataSource.capture(token, amount);
            return result;
        } catch(e){
            throw e
        }
    }

    async getx(txid)
    {

        txid = typeof(txid) == 'string' || typeof(txid) == 'number' ? txid : false;

        try{
            const result = await this.oDataSource.getx(txid);
            return result;
        } catch(e){
            throw e
        }
    }
};
