module.exports = class transactionClass {

    constructor (app, host) { 
        this.host = host;
        this.app = app;
    }

    async process(year, amount)
    {
        const host = this.host
        const oClient = await this.app.models.Client.findOne({where:{host}})
        const aUsers = await this.app.models.Customer.find({where:{passport:{inq:[oClient.id]}}})
        const aResponse = [];
        try{
            const aFilteredUsers = await aUsers.filter(oUser => oUser.anuity == year ? oUser : undefined);
            for (const oUser of aFilteredUsers) {
                const oTxTemplate = {
                    "amount": amount,
                    "status": "paid",
                    "date": new Date(`${year}-01-01T06:29:19.146Z`),
                    "type": "platform_include",
                    "clientId": oClient.id,
                    "customerId": oUser.id,
                    "tx": {
                        "items": [{
                            "object": "item",
                            "id": "6040267e6f6be4d8bf7d95e5",
                            "title": "Anuidade 2021",
                            "unit_price": amount,
                            "quantity": 1,
                            "category": null,
                            "tangible": false,
                            "venue": null,
                            "date": null
                        }]
                    }
                }
                let oResponse = await this.app.models.Transaction.findOne({
                    where: {
                        and: [
                            {type: 'platform_include'},
                            {status: 'paid'},
                            {amount: amount},
                            {date: new Date(`${year}-01-01T06:29:19.146Z`)},
                            {clientId: oClient.id},
                            {customerId: oUser.id}
                        ]
                    }
                })
                if(!oResponse){
                    oResponse = await oUser.transactions.create(oTxTemplate);
                }
                aResponse.push(oResponse);
            }
            return aResponse;
        } catch(e){
            throw e
        }
    }

}