module.exports = class wherebyClass {

    constructor (ds) { 
        this.oDataSource = ds;
    }


    async create(data)
    {
        const isLocked = typeof(data.isLocked) == 'boolean' && data.isLocked ? true : false;
        const roomNamePrefix = typeof(data.roomNamePrefix) == 'string' && data.roomNamePrefix.length > 0 ? data.roomNamePrefix : `${new Date().getMilliseconds()}-`;
        const mode = typeof(data.mode) == 'string' && ['normal','group'].indexOf(data.mode) > -1  ? data.mode : "normal";
        const startISODate = typeof(data.startISODate) == 'string' ? data.startISODate : false;
        const endISODate = typeof(data.endISODate) == 'string' ? data.endISODate : false;

        try{
            const result = await this.oDataSource.create(isLocked, roomNamePrefix, mode, startISODate, endISODate);
            return result;
        } catch(e){
            throw e
        }
    }

}